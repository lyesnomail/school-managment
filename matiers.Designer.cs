﻿namespace etablissmentt
{
    partial class matiers
    {

            /// </summary>
            
            private System.ComponentModel.IContainer components = null;

            /// <summary> 
            /// Nettoyage des ressources utilisées.
            /// </summary>
            /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }



            /// <summary> 
            /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
            /// le contenu de cette méthode avec l'éditeur de code.
            /// </summary>
            private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.nombox = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.coefbox = new Telerik.WinControls.UI.RadTextBox();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.nvscol = new Telerik.WinControls.UI.RadDropDownList();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mat = new Telerik.WinControls.UI.RadDropDownList();
            this.label5 = new System.Windows.Forms.Label();
            this.nomexam = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.typeexm = new Telerik.WinControls.UI.RadDropDownList();
            this.panel12 = new System.Windows.Forms.Panel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gridmat = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.gridTotal = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomexamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeexamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trimestreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomMatDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.examBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ecoledeformationDataSet = new etablissmentt.ecoledeformationDataSet();
            this.nomMatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coefDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomNSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.matiereBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nivscolBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.matiereTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.matiereTableAdapter();
            this.examTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.examTableAdapter();
            this.nivscolTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.nivscolTableAdapter();
            this.classeTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.classeTableAdapter();
            this.annscollBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.annscollTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.annscollTableAdapter();
            this.classeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nivscolTableAdapter1 = new etablissmentt.ecoledeformationDataSetTableAdapters.nivscolTableAdapter();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.nombox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coefbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nvscol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nomexam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeexm)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridmat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matiereBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nivscolBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.annscollBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // nombox
            // 
            this.nombox.BackColor = System.Drawing.Color.White;
            this.nombox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.matiereBindingSource, "nomMat", true));
            this.nombox.Location = new System.Drawing.Point(206, 34);
            this.nombox.Name = "nombox";
            this.nombox.Size = new System.Drawing.Size(188, 27);
            this.nombox.TabIndex = 51;
            this.nombox.TabStop = false;
            this.nombox.TextChanged += new System.EventHandler(this.nometud_TextChanged);
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nombox.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nombox.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nombox.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nombox.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(16, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 30);
            this.label3.TabIndex = 49;
            this.label3.Text = "Nom de matiers:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(16, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 30);
            this.label1.TabIndex = 82;
            this.label1.Text = "coefition ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // coefbox
            // 
            this.coefbox.BackColor = System.Drawing.Color.White;
            this.coefbox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.matiereBindingSource, "coef", true));
            this.coefbox.Location = new System.Drawing.Point(206, 88);
            this.coefbox.Name = "coefbox";
            this.coefbox.Size = new System.Drawing.Size(188, 27);
            this.coefbox.TabIndex = 52;
            this.coefbox.TabStop = false;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.coefbox.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.coefbox.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.coefbox.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 15;
            this.bunifuElipse1.TargetControl = this;
            // 
            // nvscol
            // 
            this.nvscol.BackColor = System.Drawing.Color.Teal;
            this.nvscol.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.matiereBindingSource, "nomNS", true));
            this.nvscol.DataSource = this.nivscolBindingSource;
            this.nvscol.DisplayMember = "nomNS";
            this.nvscol.EnableKeyMap = true;
            this.nvscol.Location = new System.Drawing.Point(206, 151);
            this.nvscol.Name = "nvscol";
            this.nvscol.Size = new System.Drawing.Size(188, 24);
            this.nvscol.TabIndex = 108;
            this.nvscol.ValueMember = "idNiveauScolaire";
            this.nvscol.SelectedValueChanged += new System.EventHandler(this.rech);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.nvscol.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.nvscol.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.nvscol.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.nvscol.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nvscol.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(16, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 30);
            this.label2.TabIndex = 109;
            this.label2.Text = "Niveau scolaire";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(27, 478);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 30);
            this.label4.TabIndex = 120;
            this.label4.Text = "Trimestre:";
            // 
            // mat
            // 
            this.mat.BackColor = System.Drawing.Color.Teal;
            this.mat.EnableKeyMap = true;
            radListDataItem3.Text = "Premier ";
            radListDataItem4.Text = "Deuxiéme";
            radListDataItem8.Text = "Troisiéme";
            this.mat.Items.Add(radListDataItem3);
            this.mat.Items.Add(radListDataItem4);
            this.mat.Items.Add(radListDataItem8);
            this.mat.Location = new System.Drawing.Point(206, 484);
            this.mat.Name = "mat";
            this.mat.Size = new System.Drawing.Size(188, 24);
            this.mat.TabIndex = 119;
            this.mat.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownList1_SelectedIndexChanged_1);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.mat.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.mat.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.mat.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.mat.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(27, 423);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(166, 30);
            this.label5.TabIndex = 118;
            this.label5.Text = "Type d\'Examen:";
            // 
            // nomexam
            // 
            this.nomexam.BackColor = System.Drawing.Color.White;
            this.nomexam.Location = new System.Drawing.Point(208, 368);
            this.nomexam.Name = "nomexam";
            this.nomexam.Size = new System.Drawing.Size(180, 27);
            this.nomexam.TabIndex = 116;
            this.nomexam.TabStop = false;
            this.nomexam.TextChanged += new System.EventHandler(this.radTextBox3_TextChanged);
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nomexam.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nomexam.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nomexam.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(27, 368);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(166, 30);
            this.label6.TabIndex = 115;
            this.label6.Text = "Nom d\'Examen:";
            // 
            // typeexm
            // 
            this.typeexm.BackColor = System.Drawing.Color.Teal;
            this.typeexm.EnableKeyMap = true;
            radListDataItem1.Text = "controle";
            radListDataItem2.Text = "exmain finale";
            this.typeexm.Items.Add(radListDataItem1);
            this.typeexm.Items.Add(radListDataItem2);
            this.typeexm.Location = new System.Drawing.Point(206, 428);
            this.typeexm.Name = "typeexm";
            this.typeexm.Size = new System.Drawing.Size(188, 24);
            this.typeexm.TabIndex = 121;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.typeexm.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.typeexm.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.typeexm.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.typeexm.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.typeexm.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Teal;
            this.panel12.Controls.Add(this.pictureBox11);
            this.panel12.Controls.Add(this.label17);
            this.panel12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel12.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel12.Location = new System.Drawing.Point(737, 553);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(123, 31);
            this.panel12.TabIndex = 144;
            this.panel12.Click += new System.EventHandler(this.actuexam);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::etablissmentt.Properties.Resources.united_states_win_the_white_house_hotel_business_company_refresh_icon_thumb1;
            this.pictureBox11.Location = new System.Drawing.Point(3, 4);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(29, 24);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 109;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Click += new System.EventHandler(this.actuexam);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label17.Location = new System.Drawing.Point(37, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Actualiser";
            this.label17.Click += new System.EventHandler(this.actuexam);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Teal;
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(602, 553);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(123, 31);
            this.panel3.TabIndex = 143;
            this.panel3.Click += new System.EventHandler(this.suppersio);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::etablissmentt.Properties.Resources.waste_bin1;
            this.pictureBox5.Location = new System.Drawing.Point(3, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(29, 24);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 109;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.suppersio);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(32, 4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Supprimer";
            this.label13.Click += new System.EventHandler(this.suppersio);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Teal;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(467, 553);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(123, 31);
            this.panel2.TabIndex = 142;
            this.panel2.Click += new System.EventHandler(this.modifyexm);
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::etablissmentt.Properties.Resources.white_pencil_icon_png_3_white_pencil_png_256_256;
            this.pictureBox2.Location = new System.Drawing.Point(3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(29, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 109;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.modifyexm);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label12.Location = new System.Drawing.Point(36, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "Modfier";
            this.label12.Click += new System.EventHandler(this.modifyexm);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(332, 553);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(123, 31);
            this.panel1.TabIndex = 141;
            this.panel1.Click += new System.EventHandler(this.ajouexam);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::etablissmentt.Properties.Resources.add11;
            this.pictureBox4.Location = new System.Drawing.Point(3, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(29, 24);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 109;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.ajouexam);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label11.Location = new System.Drawing.Point(39, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ajouter";
            this.label11.Click += new System.EventHandler(this.ajouexam);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Teal;
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel4.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(737, 263);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(123, 31);
            this.panel4.TabIndex = 148;
            this.panel4.Click += new System.EventHandler(this.actulisgridmat);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::etablissmentt.Properties.Resources.united_states_win_the_white_house_hotel_business_company_refresh_icon_thumb1;
            this.pictureBox1.Location = new System.Drawing.Point(3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 109;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.actulisgridmat);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label7.Location = new System.Drawing.Point(37, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Actualiser";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Teal;
            this.panel5.Controls.Add(this.pictureBox3);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel5.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.Location = new System.Drawing.Point(602, 263);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(123, 31);
            this.panel5.TabIndex = 147;
            this.panel5.Click += new System.EventHandler(this.supp);
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::etablissmentt.Properties.Resources.waste_bin1;
            this.pictureBox3.Location = new System.Drawing.Point(3, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(29, 24);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 109;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.supp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label8.Location = new System.Drawing.Point(32, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Supprimer";
            this.label8.Click += new System.EventHandler(this.supp);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Teal;
            this.panel6.Controls.Add(this.pictureBox6);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel6.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel6.Location = new System.Drawing.Point(467, 263);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(123, 31);
            this.panel6.TabIndex = 146;
            this.panel6.Click += new System.EventHandler(this.modifymat);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::etablissmentt.Properties.Resources.white_pencil_icon_png_3_white_pencil_png_256_256;
            this.pictureBox6.Location = new System.Drawing.Point(3, 4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(29, 24);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 109;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.modifymat);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label9.Location = new System.Drawing.Point(36, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Modfier";
            this.label9.Click += new System.EventHandler(this.modifymat);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Teal;
            this.panel7.Controls.Add(this.pictureBox7);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel7.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(332, 263);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(123, 31);
            this.panel7.TabIndex = 145;
            this.panel7.Click += new System.EventHandler(this.ajoutmat);
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::etablissmentt.Properties.Resources.add11;
            this.pictureBox7.Location = new System.Drawing.Point(3, 4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(29, 24);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 109;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.ajoutmat);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label10.Location = new System.Drawing.Point(39, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ajouter";
            this.label10.Click += new System.EventHandler(this.ajoutmat);
            // 
            // gridmat
            // 
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridmat.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle22;
            this.gridmat.AutoGenerateColumns = false;
            this.gridmat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridmat.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridmat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridmat.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.LightSeaGreen;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridmat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.gridmat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridmat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.nomMatDataGridViewTextBoxColumn,
            this.coefDataGridViewTextBoxColumn,
            this.nomNSDataGridViewTextBoxColumn});
            this.gridmat.DataSource = this.matiereBindingSource;
            this.gridmat.DoubleBuffered = true;
            this.gridmat.EnableHeadersVisualStyles = false;
            this.gridmat.GridColor = System.Drawing.Color.Teal;
            this.gridmat.HeaderBgColor = System.Drawing.Color.Teal;
            this.gridmat.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridmat.Location = new System.Drawing.Point(451, 19);
            this.gridmat.Name = "gridmat";
            this.gridmat.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightSeaGreen;
            this.gridmat.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.gridmat.RowTemplate.Height = 24;
            this.gridmat.Size = new System.Drawing.Size(702, 237);
            this.gridmat.TabIndex = 149;
            this.gridmat.SelectionChanged += new System.EventHandler(this.bunifuCustomDataGrid1_SelectionChanged);
            // 
            // gridTotal
            // 
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridTotal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.gridTotal.AutoGenerateColumns = false;
            this.gridTotal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridTotal.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridTotal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.LightSeaGreen;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTotal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.gridTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTotal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.nomexamDataGridViewTextBoxColumn,
            this.typeexamDataGridViewTextBoxColumn,
            this.trimestreDataGridViewTextBoxColumn,
            this.nomMatDataGridViewTextBoxColumn1});
            this.gridTotal.DataSource = this.examBindingSource;
            this.gridTotal.DoubleBuffered = true;
            this.gridTotal.EnableHeadersVisualStyles = false;
            this.gridTotal.GridColor = System.Drawing.Color.Teal;
            this.gridTotal.HeaderBgColor = System.Drawing.Color.Teal;
            this.gridTotal.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridTotal.Location = new System.Drawing.Point(451, 310);
            this.gridTotal.Name = "gridTotal";
            this.gridTotal.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.LightSeaGreen;
            this.gridTotal.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.gridTotal.RowTemplate.Height = 24;
            this.gridTotal.Size = new System.Drawing.Size(702, 237);
            this.gridTotal.TabIndex = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "idmat";
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "idexam";
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            // 
            // nomexamDataGridViewTextBoxColumn
            // 
            this.nomexamDataGridViewTextBoxColumn.DataPropertyName = "nomexam";
            this.nomexamDataGridViewTextBoxColumn.HeaderText = "Examen ";
            this.nomexamDataGridViewTextBoxColumn.Name = "nomexamDataGridViewTextBoxColumn";
            // 
            // typeexamDataGridViewTextBoxColumn
            // 
            this.typeexamDataGridViewTextBoxColumn.DataPropertyName = "typeexam";
            this.typeexamDataGridViewTextBoxColumn.HeaderText = "Type";
            this.typeexamDataGridViewTextBoxColumn.Name = "typeexamDataGridViewTextBoxColumn";
            // 
            // trimestreDataGridViewTextBoxColumn
            // 
            this.trimestreDataGridViewTextBoxColumn.DataPropertyName = "trimestre";
            this.trimestreDataGridViewTextBoxColumn.HeaderText = "Trimestre";
            this.trimestreDataGridViewTextBoxColumn.Name = "trimestreDataGridViewTextBoxColumn";
            // 
            // nomMatDataGridViewTextBoxColumn1
            // 
            this.nomMatDataGridViewTextBoxColumn1.DataPropertyName = "nomMat";
            this.nomMatDataGridViewTextBoxColumn1.HeaderText = "Matiers";
            this.nomMatDataGridViewTextBoxColumn1.Name = "nomMatDataGridViewTextBoxColumn1";
            // 
            // examBindingSource
            // 
            this.examBindingSource.DataMember = "exam";
            this.examBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // ecoledeformationDataSet
            // 
            this.ecoledeformationDataSet.DataSetName = "ecoledeformationDataSet";
            this.ecoledeformationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nomMatDataGridViewTextBoxColumn
            // 
            this.nomMatDataGridViewTextBoxColumn.DataPropertyName = "nomMat";
            this.nomMatDataGridViewTextBoxColumn.HeaderText = "Matiers";
            this.nomMatDataGridViewTextBoxColumn.Name = "nomMatDataGridViewTextBoxColumn";
            // 
            // coefDataGridViewTextBoxColumn
            // 
            this.coefDataGridViewTextBoxColumn.DataPropertyName = "coef";
            this.coefDataGridViewTextBoxColumn.HeaderText = "coef";
            this.coefDataGridViewTextBoxColumn.Name = "coefDataGridViewTextBoxColumn";
            // 
            // nomNSDataGridViewTextBoxColumn
            // 
            this.nomNSDataGridViewTextBoxColumn.DataPropertyName = "nomNS";
            this.nomNSDataGridViewTextBoxColumn.HeaderText = "Niveau scolaire";
            this.nomNSDataGridViewTextBoxColumn.Name = "nomNSDataGridViewTextBoxColumn";
            // 
            // matiereBindingSource
            // 
            this.matiereBindingSource.DataMember = "matiere";
            this.matiereBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // nivscolBindingSource
            // 
            this.nivscolBindingSource.DataMember = "nivscol";
            this.nivscolBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // matiereTableAdapter
            // 
            this.matiereTableAdapter.ClearBeforeFill = true;
            // 
            // examTableAdapter
            // 
            this.examTableAdapter.ClearBeforeFill = true;
            // 
            // nivscolTableAdapter
            // 
            this.nivscolTableAdapter.ClearBeforeFill = true;
            // 
            // classeTableAdapter
            // 
            this.classeTableAdapter.ClearBeforeFill = true;
            // 
            // annscollBindingSource
            // 
            this.annscollBindingSource.DataMember = "annscoll";
            // 
            // annscollTableAdapter
            // 
            this.annscollTableAdapter.ClearBeforeFill = true;
            // 
            // classeBindingSource
            // 
            this.classeBindingSource.DataMember = "classe";
            // 
            // nivscolTableAdapter1
            // 
            this.nivscolTableAdapter1.ClearBeforeFill = true;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "nivscol";
            // 
            // matiers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.gridTotal);
            this.Controls.Add(this.gridmat);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.typeexm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.mat);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nomexam);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nvscol);
            this.Controls.Add(this.coefbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nombox);
            this.Controls.Add(this.label3);
            this.Name = "matiers";
            this.Size = new System.Drawing.Size(1173, 604);
            this.Load += new System.EventHandler(this.matiers_Load);
            this.Click += new System.EventHandler(this.actuexam);
            ((System.ComponentModel.ISupportInitialize)(this.nombox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coefbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nvscol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nomexam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeexm)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridmat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matiereBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nivscolBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.annscollBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

            }


            private Telerik.WinControls.UI.RadTextBox nombox;
            private System.Windows.Forms.Label label3;
            private System.Windows.Forms.Label label1;
            private Telerik.WinControls.UI.RadTextBox coefbox;
            private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadDropDownList nvscol;
        private Telerik.WinControls.UI.RadDropDownList typeexm;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadDropDownList mat;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadTextBox nomexam;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuCustomDataGrid gridTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn idmatDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource examBindingSource;
        private ecoledeformationDataSet ecoledeformationDataSet;
        private Bunifu.Framework.UI.BunifuCustomDataGrid gridmat;
        private System.Windows.Forms.DataGridViewTextBoxColumn idNiveauScolaireDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource matiereBindingSource;
        private ecoledeformationDataSetTableAdapters.matiereTableAdapter matiereTableAdapter;
        private ecoledeformationDataSetTableAdapters.examTableAdapter examTableAdapter;
        private System.Windows.Forms.BindingSource nivscolBindingSource;
        private ecoledeformationDataSetTableAdapters.nivscolTableAdapter nivscolTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomexamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeexamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn trimestreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomMatDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomMatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn coefDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomNSDataGridViewTextBoxColumn;
        private ecoledeformationDataSetTableAdapters.classeTableAdapter classeTableAdapter;
        private System.Windows.Forms.BindingSource annscollBindingSource;
        private ecoledeformationDataSetTableAdapters.annscollTableAdapter annscollTableAdapter;
        private System.Windows.Forms.BindingSource classeBindingSource;
        private ecoledeformationDataSetTableAdapters.nivscolTableAdapter nivscolTableAdapter1;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
    }

