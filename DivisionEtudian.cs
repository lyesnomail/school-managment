﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
namespace etablissmentt
{
    public partial class DivisionEtudian : UserControl
    {
        public DivisionEtudian()
        {
            InitializeComponent();
        }
        bool IsValidEmail(string text)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(text);
                return addr.Address == text;

            }
            catch { return false; }
        }

        private void bunifuCustomDataGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void photobtn_Click(object sender, EventArgs e)
        {

        }

        private void radTextBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void DivisionEtudian_Load(object sender, EventArgs e)
        {
            this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), "%%");          // TODO: cette ligne de code charge les données dans la table 'ecoledeformationDataSet.nivscol'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.nivscolTableAdapter.Fill(this.ecoledeformationDataSet1.nivscol);
            // TODO: cette ligne de code charge les données dans la table 'ecoledeformationDataSet.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.classeTableAdapter.Fill(this.ecoledeformationDataSet1.classe);
            // TODO: cette ligne de code charge les données dans la table 'ecoledeformationDataSet.annscoll'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.annscollTableAdapter.Fill(this.ecoledeformationDataSet1.annscoll);

           int id= Int32.Parse(this.etudiantTableAdapter.minid().ToString());
           
            byte[] img = this.etudiantTableAdapter.img(id);
           if (img != null)
           {
               MemoryStream ms = new MemoryStream(img);
               pictureBox1.Image = Image.FromStream(ms);
           }

           string ge = etudiantTableAdapter.gen(id);
           if (ge != null)
           {
               if (ge == "Garçon") radioButton1.Checked = true;
               else radioButton2.Checked = true;
                
           }

            for (int i = 0; i < gridetud.Columns.Count; i++)
                if (gridetud.Columns[i] is DataGridViewImageColumn)
                {
                    ((DataGridViewImageColumn)gridetud.Columns[i]).ImageLayout = DataGridViewImageCellLayout.Stretch;

                    break;
                }
        }

        private void label14_Click(object sender, EventArgs e)
        {
            fabssance a = new fabssance();
            a.Show();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void mod(object sender, EventArgs e)
        {
            etudiantBindingSource.EndEdit();
            etudiantTableAdapter.Update(ecoledeformationDataSet);

            if (nometud.Text == "") { MessageBox.Show("vous devez remplir le nom"); nometud.Focus(); }
            else if (prenometud.Text == "") { MessageBox.Show("vous devez remplir le nom"); prenometud.Focus(); }
            else if (dateetud.Text == "") { MessageBox.Show("vous devez remplir la date de naiscance"); dateetud.Focus(); }
            else if (lieunaissetud.Text == "") { MessageBox.Show("vous devez remplir le lieu de naissnace"); lieunaissetud.Focus(); }
            else if (adressetud.Text == "") { MessageBox.Show("vous devez remplir l'adress d'etudiant"); adressetud.Focus(); }
            else if (emailetud.Text == "" || !IsValidEmail(emailetud.Text)) { MessageBox.Show("vous devez remplir une adress email juste"); emailetud.Focus(); }
            else
            {


                string p;
                if (dateetud.Value.ToShortDateString().Substring(8, 1) == "0")
                    p = dateetud.Value.ToShortDateString().Substring(9, 1) + "___";
                else
                    p = dateetud.Value.ToShortDateString().Substring(8, 2) + "___";
                int v;

                if (etudiantTableAdapter.matricule(p) != null)
                    v = Int32.Parse(etudiantTableAdapter.matricule(p)) + 1;
                else v = Int32.Parse(dateetud.Value.ToShortDateString().Substring(8, 2) + "000");

                int id = int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString());
                MemoryStream ms = new MemoryStream();
                pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                byte[] img = ms.ToArray();
               
                etudiantTableAdapter.matinsert(v.ToString(), img, genre.SelectedValue.ToString(), id);
                etudiantBindingSource.EndEdit();
                //this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, "%%", "1", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%");
                this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, "%%", "1", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%",  DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), "%%");
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Choose Image(*.jpg; *.png; *.gif)|*.jpg; *.png; *.gif";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(opf.FileName);
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {
            if (gridetud.Rows.Count > 0)
            {
                DialogResult d = MessageBox.Show("vous voulez vraiment supprimer l'etudiant?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (d.ToString().CompareTo("OK") == 0)
                {
                    int id = int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString());

                    this.etudiantTableAdapter.Delete(id);
                    this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, "%%", "1", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), "%%");
                }

            }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void enter(object sender, EventArgs e)
        {
            pictureBox3.Visible = true;
            lab.Visible = true;

        }

        private void sort(object sender, EventArgs e)
        {
            pictureBox3.Visible = false; lab.Visible = false;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void lab_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ve(object sender, EventArgs e)
        {

            if (label11.Text == "Ajouter")
            {
                label11.Text = "Confirmer";
               
                etudiantBindingSource.AddNew();
                etudiantBindingSource.ResetCurrentItem();
            }
            else
            {
                etudiantBindingSource.EndEdit();
                etudiantTableAdapter.Update(ecoledeformationDataSet);

                if (nometud.Text == "") { MessageBox.Show("vous devez remplir le nom"); nometud.Focus(); }
                else if (prenometud.Text == "") { MessageBox.Show("vous devez remplir le nom"); prenometud.Focus(); }
                else if (dateetud.Text == "") { MessageBox.Show("vous devez remplir la date de naiscance"); dateetud.Focus(); }
                else if (lieunaissetud.Text == "") { MessageBox.Show("vous devez remplir le lieu de naissnace"); lieunaissetud.Focus(); }
                else if (adressetud.Text == "") { MessageBox.Show("vous devez remplir l'adress d'etudiant"); adressetud.Focus(); }
                else if (emailetud.Text == "" || !IsValidEmail(emailetud.Text)) { MessageBox.Show("vous devez remplir une adress email juste"); emailetud.Focus(); }

                else
                {


                    string p;
                    if (dateetud.Value.ToShortDateString().Substring(8, 1) == "0")
                        p = dateetud.Value.ToShortDateString().Substring(9, 1) + "___";
                    else
                        p = dateetud.Value.ToShortDateString().Substring(8, 2) + "___";
                    int v;
                    if (etudiantTableAdapter.matricule(p) != null)
                        v = Int32.Parse(etudiantTableAdapter.matricule(p)) + 1;
                    else v = Int32.Parse(dateetud.Value.ToShortDateString().Substring(8, 2) + "000");

                    int id = Int32.Parse(etudiantTableAdapter.maxid().ToString());
                    MemoryStream ms = new MemoryStream();
                    pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                    byte[] img = ms.ToArray();
                    etudiantTableAdapter.matinsert(v.ToString(), img, genre.SelectedValue.ToString(), id);
                    etudiantBindingSource.EndEdit();
                    this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), "%%");
                    label11.Text = "Ajouter";

                }
            }
        }

        private void adressetud_TextChanged(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {
            this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), "%%");
        }

        private void label19_Click(object sender, EventArgs e)
        {
            //this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, av.SelectedValue.ToString(), mat.Text, cls.SelectedValue.ToString(), nv.SelectedValue.ToString());
        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {

        }

        private void annscoll_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {






        }

        private void rehcerche_click(object sender, EventArgs e)
        {
            string av1, nv1, cls1, mat1 ;
            if (av.Text != null) av1 = av.ValueMember; else av1 = "%%";
            if (nv.Text != null) nv1 = nv.ValueMember; else nv1 = "%%";
            if (cls.Text != null) cls1 = cls.ValueMember; else cls1 = "%%";
            if (mat.Text != null) mat1 = mat.Text; else mat1 = "%%";
            this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, mat1, "%%", "%%", "%%", "%%", "%%", "%%", "%%", av1, nv1, DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), "%%");
        }

        private void actualisation(object sender, EventArgs e)
        {
            this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), "%%");
        }

        private void absance(object sender, EventArgs e)
        {

        }

        private void rech(object sender, EventArgs e)
        {
            //string  nom1, prnm1, dat, lieu, adr,gende,numtel,maill,mat1;
            //if (mat.Text != "") mat1 = mat.Text; else mat1 = "%%";
            //if (nometud.Text != "") nom1 = nometud.Text; else nom1 = "%%";
            //if (prenometud.Text != "") prnm1 = mat.Text; else prnm1 = "%%";
            ////if (dateetud.Text != null) dat = dateetud.Text; else dat = "%%";
            //if (adressetud.Text != "") adr = adressetud.Text; else adr = "%%";
            //if (lieunaissetud.Text != "") lieu = lieunaissetud.Text; else lieu = "%%";
            //if (genre.Text != "") gende = genre.SelectedValue; else gende = "%%";
            //if (tel.Text != "") numtel = tel.Text; else numtel = "%%";
            //if (emailetud.Text != "") maill = emailetud.Text; else maill = "%%";
            //this.etudiantTableAdapter.Fill(this.ecoledeformationDataSet.etudiant, mat1, "1",lieu, nom1, adr,maill,numtel,prnm1,"%%", "%%", "%%", "%%");
        }

        private void h(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            string nom1, prnm1, dat, lieu, adr, gende, numtel, maill, mat1;
            if (mat.Text != "") mat1 = mat.Text; else mat1 = "%%";
            if (nometud.Text != "") nom1 = nometud.Text; else nom1 = "%%";
            if (prenometud.Text != "") prnm1 = mat.Text; else prnm1 = "%%";
            //if (dateetud.Text != null) dat = dateetud.Text; else dat = "%%";
            if (adressetud.Text != "") adr = adressetud.Text; else adr = "%%";
            if (lieunaissetud.Text != "") lieu = lieunaissetud.Text; else lieu = "%%";
            if (genre.Text != "") gende = genre.SelectedValue; else gende = "%%";
            if (tel.Text != "") numtel = tel.Text; else numtel = "%%";
            if (emailetud.Text != "") maill = emailetud.Text; else maill = "%%";
       
        }

        private void selctiogrid(object sender, DataGridViewRowEventArgs e)
        {
            byte[] img = this.etudiantTableAdapter.img(int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString()));
            MemoryStream ms = new MemoryStream(img);
            pictureBox1.Image = Image.FromStream(ms);
            MessageBox.Show("ll");
        }

        private void t(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show("ll");
        }

        private void f(object sender, DataGridViewCellMouseEventArgs e)
        {
            byte[] img = this.etudiantTableAdapter.img(int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString()));
            if (img != null)
            {
                MemoryStream ms = new MemoryStream(img);
                pictureBox1.Image = Image.FromStream(ms);
            }
            else pictureBox1.Image = etablissmentt.Properties.Resources.t;
            string ge = etudiantTableAdapter.gen(int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString()));
            if (ge != null)
            {
                if (ge == "Garçon") radioButton1.Checked = true;
                else radioButton2.Checked = true;
            }
         
        }

        private void imprimer_click(object sender, EventArgs e) 
        {
            if (gridetud.Rows.Count > 0) 
            {
                int temp_id_etudiant = int.Parse(gridetud.SelectedRows[0].Cells["idEtud"].Value.ToString() );
                etudiantTableAdapter.Fill(ecoledeformationDataSet.etudiant, "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%",  DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), temp_id_etudiant.ToString());
                
            }
        }


        }

       
    }
