﻿using System.Windows.Forms;

namespace etablissmentt
{
    partial class fabssance


    { 

            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }



            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fabssance));
            this.gridTotal = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.hide = new System.Windows.Forms.Label();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lb5 = new System.Windows.Forms.Label();
            this.dateetud = new Telerik.WinControls.UI.RadDateTimePicker();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.ecoledeformationDataSet = new etablissmentt.ecoledeformationDataSet();
            this.ecoledeformationDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.verssementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.verssementTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.verssementTableAdapter();
            this.abssanceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.abssanceTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.abssanceTableAdapter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.idabssDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateabssDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.justifierDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heurDebutDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heurFinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.verssementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abssanceBindingSource)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridTotal
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridTotal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridTotal.AutoGenerateColumns = false;
            this.gridTotal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridTotal.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridTotal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTotal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTotal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idabssDataGridViewTextBoxColumn,
            this.dateabssDataGridViewTextBoxColumn,
            this.justifierDataGridViewTextBoxColumn,
            this.heurDebutDataGridViewTextBoxColumn,
            this.heurFinDataGridViewTextBoxColumn});
            this.gridTotal.DataSource = this.abssanceBindingSource;
            this.gridTotal.DoubleBuffered = true;
            this.gridTotal.EnableHeadersVisualStyles = false;
            this.gridTotal.HeaderBgColor = System.Drawing.Color.Teal;
            this.gridTotal.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridTotal.Location = new System.Drawing.Point(471, 57);
            this.gridTotal.Name = "gridTotal";
            this.gridTotal.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gridTotal.RowTemplate.Height = 24;
            this.gridTotal.Size = new System.Drawing.Size(429, 341);
            this.gridTotal.TabIndex = 76;
            // 
            // hide
            // 
            this.hide.AutoSize = true;
            this.hide.BackColor = System.Drawing.Color.Transparent;
            this.hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hide.ForeColor = System.Drawing.Color.Teal;
            this.hide.Location = new System.Drawing.Point(808, 3);
            this.hide.Name = "hide";
            this.hide.Size = new System.Drawing.Size(34, 44);
            this.hide.TabIndex = 117;
            this.hide.Text = "-";
            // 
            // radLabel1
            // 
            this.radLabel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radLabel1.Font = new System.Drawing.Font("Gill Sans MT", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.Teal;
            this.radLabel1.Location = new System.Drawing.Point(849, 6);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(31, 33);
            this.radLabel1.TabIndex = 116;
            this.radLabel1.Text = "X";
            // 
            // lb5
            // 
            this.lb5.AutoSize = true;
            this.lb5.BackColor = System.Drawing.Color.Transparent;
            this.lb5.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb5.ForeColor = System.Drawing.Color.Teal;
            this.lb5.Location = new System.Drawing.Point(102, 23);
            this.lb5.Name = "lb5";
            this.lb5.Size = new System.Drawing.Size(227, 38);
            this.lb5.TabIndex = 128;
            this.lb5.Text = "Date d\'abssnace:";
            // 
            // dateetud
            // 
            this.dateetud.ForeColor = System.Drawing.Color.Teal;
            this.dateetud.Location = new System.Drawing.Point(109, 74);
            this.dateetud.Name = "dateetud";
            // 
            // 
            // 
            this.dateetud.RootElement.ControlBounds = new System.Drawing.Rectangle(109, 74, 205, 25);
            this.dateetud.Size = new System.Drawing.Size(226, 24);
            this.dateetud.TabIndex = 133;
            this.dateetud.TabStop = false;
            this.dateetud.Text = "mercredi 27 mars 2019";
            this.dateetud.Value = new System.DateTime(2019, 3, 27, 0, 0, 0, 0);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.dateetud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor3 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.Teal;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Teal;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(88, 447);
            this.bunifuGradientPanel1.TabIndex = 77;
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.abssanceBindingSource, "heurDebut", true));
            this.radDateTimePicker1.ForeColor = System.Drawing.Color.Teal;
            this.radDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.radDateTimePicker1.Location = new System.Drawing.Point(109, 162);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            // 
            // 
            // 
            this.radDateTimePicker1.RootElement.ControlBounds = new System.Drawing.Rectangle(109, 162, 205, 25);
            this.radDateTimePicker1.Size = new System.Drawing.Size(226, 24);
            this.radDateTimePicker1.TabIndex = 134;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "00:00:00";
            this.radDateTimePicker1.Value = new System.DateTime(2019, 6, 27, 0, 0, 0, 0);
            this.radDateTimePicker1.Visible = false;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor3 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.abssanceBindingSource, "heurFin", true));
            this.radDateTimePicker2.ForeColor = System.Drawing.Color.Teal;
            this.radDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.radDateTimePicker2.Location = new System.Drawing.Point(109, 250);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            // 
            // 
            // 
            this.radDateTimePicker2.RootElement.ControlBounds = new System.Drawing.Rectangle(109, 250, 205, 25);
            this.radDateTimePicker2.Size = new System.Drawing.Size(226, 24);
            this.radDateTimePicker2.TabIndex = 135;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.Text = "00:00:00";
            this.radDateTimePicker2.Value = new System.DateTime(2019, 3, 27, 0, 0, 0, 0);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDateTimePicker2.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDateTimePicker2.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDateTimePicker2.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDateTimePicker2.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor3 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDateTimePicker2.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDateTimePicker2.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(104, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 38);
            this.label1.TabIndex = 136;
            this.label1.Text = "heur de fin";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(107, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 38);
            this.label2.TabIndex = 137;
            this.label2.Text = "heur de debut:";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radioButton2);
            this.radGroupBox1.Controls.Add(this.radioButton1);
            this.radGroupBox1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.HeaderText = "Etat";
            this.radGroupBox1.Location = new System.Drawing.Point(114, 299);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(221, 72);
            this.radGroupBox1.TabIndex = 139;
            this.radGroupBox1.Text = "Etat";
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).InvalidateMeasureInMainLayout = 2;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).BorderColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).BorderColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).BackColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radGroupBox1.GetChildAt(0).GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Standard;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).HighlightColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radGroupBox1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radGroupBox1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Bold);
            this.radioButton2.ForeColor = System.Drawing.Color.Teal;
            this.radioButton2.Location = new System.Drawing.Point(117, 36);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(97, 19);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Non justifier";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Bold);
            this.radioButton1.ForeColor = System.Drawing.Color.Teal;
            this.radioButton1.Location = new System.Drawing.Point(24, 36);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(73, 19);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Justifier";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // ecoledeformationDataSet
            // 
            this.ecoledeformationDataSet.DataSetName = "ecoledeformationDataSet";
            this.ecoledeformationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ecoledeformationDataSetBindingSource
            // 
            this.ecoledeformationDataSetBindingSource.DataSource = this.ecoledeformationDataSet;
            this.ecoledeformationDataSetBindingSource.Position = 0;
            // 
            // verssementBindingSource
            // 
            this.verssementBindingSource.DataMember = "verssement";
            this.verssementBindingSource.DataSource = this.ecoledeformationDataSetBindingSource;
            // 
            // verssementTableAdapter
            // 
            this.verssementTableAdapter.ClearBeforeFill = true;
            // 
            // abssanceBindingSource
            // 
            this.abssanceBindingSource.DataMember = "abssance";
            this.abssanceBindingSource.DataSource = this.ecoledeformationDataSetBindingSource;
            // 
            // abssanceTableAdapter
            // 
            this.abssanceTableAdapter.ClearBeforeFill = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Teal;
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(439, 401);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(123, 31);
            this.panel3.TabIndex = 142;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::etablissmentt.Properties.Resources.waste_bin1;
            this.pictureBox5.Location = new System.Drawing.Point(3, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(29, 24);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 109;
            this.pictureBox5.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(32, 4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Supprimer";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Teal;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(305, 401);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(123, 31);
            this.panel2.TabIndex = 141;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::etablissmentt.Properties.Resources.white_pencil_icon_png_3_white_pencil_png_256_256;
            this.pictureBox2.Location = new System.Drawing.Point(3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(29, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 109;
            this.pictureBox2.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label12.Location = new System.Drawing.Point(36, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "Modfier";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(170, 401);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(123, 31);
            this.panel1.TabIndex = 140;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::etablissmentt.Properties.Resources.add11;
            this.pictureBox4.Location = new System.Drawing.Point(3, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(29, 24);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 109;
            this.pictureBox4.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label11.Location = new System.Drawing.Point(39, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ajouter";
            // 
            // idabssDataGridViewTextBoxColumn
            // 
            this.idabssDataGridViewTextBoxColumn.DataPropertyName = "idabss";
            this.idabssDataGridViewTextBoxColumn.HeaderText = "idabss";
            this.idabssDataGridViewTextBoxColumn.Name = "idabssDataGridViewTextBoxColumn";
            // 
            // dateabssDataGridViewTextBoxColumn
            // 
            this.dateabssDataGridViewTextBoxColumn.DataPropertyName = "date_abss";
            this.dateabssDataGridViewTextBoxColumn.HeaderText = "date_abss";
            this.dateabssDataGridViewTextBoxColumn.Name = "dateabssDataGridViewTextBoxColumn";
            // 
            // justifierDataGridViewTextBoxColumn
            // 
            this.justifierDataGridViewTextBoxColumn.DataPropertyName = "justifier";
            this.justifierDataGridViewTextBoxColumn.HeaderText = "justifier";
            this.justifierDataGridViewTextBoxColumn.Name = "justifierDataGridViewTextBoxColumn";
            // 
            // heurDebutDataGridViewTextBoxColumn
            // 
            this.heurDebutDataGridViewTextBoxColumn.DataPropertyName = "heurDebut";
            this.heurDebutDataGridViewTextBoxColumn.HeaderText = "heurDebut";
            this.heurDebutDataGridViewTextBoxColumn.Name = "heurDebutDataGridViewTextBoxColumn";
            // 
            // heurFinDataGridViewTextBoxColumn
            // 
            this.heurFinDataGridViewTextBoxColumn.DataPropertyName = "heurFin";
            this.heurFinDataGridViewTextBoxColumn.HeaderText = "heurFin";
            this.heurFinDataGridViewTextBoxColumn.Name = "heurFinDataGridViewTextBoxColumn";
            // 
            // fabssance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(933, 440);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radDateTimePicker2);
            this.Controls.Add(this.radDateTimePicker1);
            this.Controls.Add(this.dateetud);
            this.Controls.Add(this.lb5);
            this.Controls.Add(this.hide);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Controls.Add(this.gridTotal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "fabssance";
            this.Load += new System.EventHandler(this.fabssance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.verssementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abssanceBindingSource)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

            }
            private Bunifu.Framework.UI.BunifuCustomDataGrid gridTotal;
            private System.Windows.Forms.BindingSource etudierBindingSource;
            private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
            private System.Windows.Forms.Label hide;
            private Telerik.WinControls.UI.RadLabel radLabel1;
            private Label lb5;
            private Telerik.WinControls.UI.RadDateTimePicker dateetud;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
        private Label label1;
        private Label label2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private RadioButton radioButton2;
        private RadioButton radioButton1;
        private BindingSource ecoledeformationDataSetBindingSource;
        private ecoledeformationDataSet ecoledeformationDataSet;
        private BindingSource verssementBindingSource;
        private ecoledeformationDataSetTableAdapters.verssementTableAdapter verssementTableAdapter;
        private BindingSource abssanceBindingSource;
        private ecoledeformationDataSetTableAdapters.abssanceTableAdapter abssanceTableAdapter;
        private Panel panel3;
        private PictureBox pictureBox5;
        private Label label13;
        private Panel panel2;
        private PictureBox pictureBox2;
        private Label label12;
        private Panel panel1;
        private PictureBox pictureBox4;
        private Label label11;
        private DataGridViewTextBoxColumn idabssDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn dateabssDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn justifierDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn heurDebutDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn heurFinDataGridViewTextBoxColumn;
    }
    }