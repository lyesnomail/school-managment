﻿namespace etablissmentt
{
    partial class divisionProf
    {




            /// <summary> 
            /// Variable nécessaire au concepteur.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary> 
            /// Nettoyage des ressources utilisées.
            /// </summary>
            /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }



            /// <summary> 
            /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
            /// le contenu de cette méthode avec l'éditeur de code.
            /// </summary>
            private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(divisionProf));
            this.prenometud = new Telerik.WinControls.UI.RadTextBox();
            this.professeurBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ecoledeformationDataSet = new etablissmentt.ecoledeformationDataSet();
            this.nometud = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateetud = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lieunaissetud = new Telerik.WinControls.UI.RadTextBox();
            this.emailetud = new Telerik.WinControls.UI.RadTextBox();
            this.grad = new Telerik.WinControls.UI.RadTextBox();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.adressetud = new Telerik.WinControls.UI.RadTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lab = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.gridetud = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.professeurTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.professeurTableAdapter();
            this.impbtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.report1 = new FastReport.Report();
            this.idProfDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gradeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDeNaissanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lieuDeNaissanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.photo = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.prenometud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.professeurBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nometud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lieunaissetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grad)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adressetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            this.SuspendLayout();
            // 
            // prenometud
            // 
            this.prenometud.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prenometud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "Prenom", true));
            this.prenometud.Location = new System.Drawing.Point(221, 180);
            this.prenometud.Margin = new System.Windows.Forms.Padding(2);
            this.prenometud.Name = "prenometud";
            // 
            // 
            // 
            this.prenometud.RootElement.ControlBounds = new System.Drawing.Rectangle(221, 180, 100, 20);
            this.prenometud.RootElement.StretchVertically = true;
            this.prenometud.Size = new System.Drawing.Size(135, 22);
            this.prenometud.TabIndex = 52;
            this.prenometud.TabStop = false;
            this.prenometud.Leave += new System.EventHandler(this.leave);
            // 
            // professeurBindingSource
            // 
            this.professeurBindingSource.DataMember = "professeur";
            this.professeurBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // ecoledeformationDataSet
            // 
            this.ecoledeformationDataSet.DataSetName = "ecoledeformationDataSet";
            this.ecoledeformationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nometud
            // 
            this.nometud.BackColor = System.Drawing.Color.White;
            this.nometud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "Nom", true));
            this.nometud.Location = new System.Drawing.Point(14, 180);
            this.nometud.Margin = new System.Windows.Forms.Padding(2);
            this.nometud.Name = "nometud";
            // 
            // 
            // 
            this.nometud.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 180, 100, 20);
            this.nometud.RootElement.StretchVertically = true;
            this.nometud.Size = new System.Drawing.Size(135, 22);
            this.nometud.TabIndex = 51;
            this.nometud.TabStop = false;
            this.nometud.Leave += new System.EventHandler(this.leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(218, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 21);
            this.label4.TabIndex = 50;
            this.label4.Text = "Prenom:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(10, 143);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 24);
            this.label3.TabIndex = 49;
            this.label3.Text = "Nom:";
            // 
            // dateetud
            // 
            this.dateetud.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dateetud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "Date de naissance", true));
            this.dateetud.ForeColor = System.Drawing.Color.Teal;
            this.dateetud.Location = new System.Drawing.Point(14, 246);
            this.dateetud.Margin = new System.Windows.Forms.Padding(2);
            this.dateetud.Name = "dateetud";
            // 
            // 
            // 
            this.dateetud.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 246, 164, 20);
            this.dateetud.RootElement.StretchVertically = true;
            this.dateetud.Size = new System.Drawing.Size(135, 20);
            this.dateetud.TabIndex = 80;
            this.dateetud.TabStop = false;
            this.dateetud.Text = "mercredi 27 mars 2019";
            this.dateetud.Value = new System.DateTime(2019, 3, 27, 0, 0, 0, 0);
            this.dateetud.Leave += new System.EventHandler(this.leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label7.ForeColor = System.Drawing.Color.Teal;
            this.label7.Location = new System.Drawing.Point(218, 214);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 21);
            this.label7.TabIndex = 77;
            this.label7.Text = "Lieu de naissnace:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(10, 214);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 21);
            this.label5.TabIndex = 76;
            this.label5.Text = "Date de Naissace:";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGroupBox1.Controls.Add(this.radioButton2);
            this.radGroupBox1.Controls.Add(this.radioButton1);
            this.radGroupBox1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.HeaderText = "genre";
            this.radGroupBox1.Location = new System.Drawing.Point(214, 341);
            this.radGroupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(2, 15, 2, 2);
            // 
            // 
            // 
            this.radGroupBox1.RootElement.ControlBounds = new System.Drawing.Rectangle(214, 341, 200, 100);
            this.radGroupBox1.Size = new System.Drawing.Size(141, 76);
            this.radGroupBox1.TabIndex = 81;
            this.radGroupBox1.Text = "genre";
            this.radGroupBox1.Leave += new System.EventHandler(this.leave);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "gender", true));
            this.radioButton2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.ForeColor = System.Drawing.Color.Teal;
            this.radioButton2.Location = new System.Drawing.Point(71, 47);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(65, 23);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "MMe.";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "gender", true));
            this.radioButton1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.ForeColor = System.Drawing.Color.Teal;
            this.radioButton1.Location = new System.Drawing.Point(71, 20);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(50, 23);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Mr.";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(10, 278);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 21);
            this.label1.TabIndex = 82;
            this.label1.Text = "E-mail:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(10, 344);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 21);
            this.label2.TabIndex = 84;
            this.label2.Text = "grade:";
            // 
            // lieunaissetud
            // 
            this.lieunaissetud.BackColor = System.Drawing.Color.White;
            this.lieunaissetud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "Lieu de naissance", true));
            this.lieunaissetud.Location = new System.Drawing.Point(221, 244);
            this.lieunaissetud.Margin = new System.Windows.Forms.Padding(2);
            this.lieunaissetud.Name = "lieunaissetud";
            // 
            // 
            // 
            this.lieunaissetud.RootElement.ControlBounds = new System.Drawing.Rectangle(221, 244, 100, 20);
            this.lieunaissetud.RootElement.StretchVertically = true;
            this.lieunaissetud.Size = new System.Drawing.Size(135, 22);
            this.lieunaissetud.TabIndex = 103;
            this.lieunaissetud.TabStop = false;
            this.lieunaissetud.Leave += new System.EventHandler(this.leave);
            // 
            // emailetud
            // 
            this.emailetud.BackColor = System.Drawing.Color.White;
            this.emailetud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "Mail", true));
            this.emailetud.Location = new System.Drawing.Point(14, 312);
            this.emailetud.Margin = new System.Windows.Forms.Padding(2);
            this.emailetud.Name = "emailetud";
            // 
            // 
            // 
            this.emailetud.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 312, 100, 20);
            this.emailetud.RootElement.StretchVertically = true;
            this.emailetud.Size = new System.Drawing.Size(135, 22);
            this.emailetud.TabIndex = 52;
            this.emailetud.TabStop = false;
            this.emailetud.Leave += new System.EventHandler(this.leave);
            // 
            // grad
            // 
            this.grad.BackColor = System.Drawing.Color.White;
            this.grad.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "Grade", true));
            this.grad.Location = new System.Drawing.Point(14, 367);
            this.grad.Margin = new System.Windows.Forms.Padding(2);
            this.grad.Name = "grad";
            // 
            // 
            // 
            this.grad.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 367, 100, 20);
            this.grad.RootElement.StretchVertically = true;
            this.grad.Size = new System.Drawing.Size(135, 22);
            this.grad.TabIndex = 104;
            this.grad.TabStop = false;
            this.grad.Leave += new System.EventHandler(this.leave);
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 15;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Teal;
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(436, 438);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(112, 25);
            this.panel3.TabIndex = 113;
            this.panel3.Click += new System.EventHandler(this.b);
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::etablissmentt.Properties.Resources.waste_bin1;
            this.pictureBox5.Location = new System.Drawing.Point(4, 4);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(17, 16);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 109;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.b);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(24, 3);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "Supprimer";
            this.label13.Click += new System.EventHandler(this.b);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Teal;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(316, 438);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(112, 25);
            this.panel2.TabIndex = 112;
            this.panel2.Click += new System.EventHandler(this.f);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::etablissmentt.Properties.Resources.white_pencil_icon_png_3_white_pencil_png_256_256;
            this.pictureBox2.Location = new System.Drawing.Point(2, 3);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(22, 20);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 109;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.f);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label12.Location = new System.Drawing.Point(27, 4);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "Modfier";
            this.label12.Click += new System.EventHandler(this.f);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(195, 438);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(112, 25);
            this.panel1.TabIndex = 111;
            this.panel1.Click += new System.EventHandler(this.x);
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::etablissmentt.Properties.Resources.add11;
            this.pictureBox4.Location = new System.Drawing.Point(2, 3);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(22, 20);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 109;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.x);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label11.Location = new System.Drawing.Point(29, 4);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ajouter";
            this.label11.Click += new System.EventHandler(this.x);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Teal;
            this.panel4.Controls.Add(this.pictureBox6);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel4.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(557, 438);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(112, 25);
            this.panel4.TabIndex = 114;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::etablissmentt.Properties.Resources.news1;
            this.pictureBox6.Location = new System.Drawing.Point(2, 3);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(22, 20);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 109;
            this.pictureBox6.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Location = new System.Drawing.Point(24, 3);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Journal";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Teal;
            this.panel10.Controls.Add(this.pictureBox12);
            this.panel10.Controls.Add(this.label20);
            this.panel10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel10.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel10.Location = new System.Drawing.Point(634, 42);
            this.panel10.Margin = new System.Windows.Forms.Padding(2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(92, 25);
            this.panel10.TabIndex = 119;
            this.panel10.Click += new System.EventHandler(this.click);
            this.panel10.Paint += new System.Windows.Forms.PaintEventHandler(this.panel10_Paint);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::etablissmentt.Properties.Resources.united_states_win_the_white_house_hotel_business_company_refresh_icon_thumb1;
            this.pictureBox12.Location = new System.Drawing.Point(2, 3);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(22, 20);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 109;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Click += new System.EventHandler(this.click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label20.Location = new System.Drawing.Point(28, 4);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 17);
            this.label20.TabIndex = 0;
            this.label20.Text = "Actualiser";
            this.label20.Click += new System.EventHandler(this.click);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Teal;
            this.panel9.Controls.Add(this.pictureBox11);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel9.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel9.Location = new System.Drawing.Point(527, 42);
            this.panel9.Margin = new System.Windows.Forms.Padding(2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(92, 25);
            this.panel9.TabIndex = 118;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::etablissmentt.Properties.Resources._66937556_magnifying_glass_lupe_icon_vector_illustration_graphic_design;
            this.pictureBox11.Location = new System.Drawing.Point(2, 3);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(22, 20);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 109;
            this.pictureBox11.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label19.Location = new System.Drawing.Point(21, 4);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 17);
            this.label19.TabIndex = 0;
            this.label19.Text = "Rechercher";
            // 
            // adressetud
            // 
            this.adressetud.BackColor = System.Drawing.Color.White;
            this.adressetud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.professeurBindingSource, "Adresse", true));
            this.adressetud.Location = new System.Drawing.Point(221, 312);
            this.adressetud.Margin = new System.Windows.Forms.Padding(2);
            this.adressetud.Name = "adressetud";
            // 
            // 
            // 
            this.adressetud.RootElement.ControlBounds = new System.Drawing.Rectangle(221, 312, 100, 20);
            this.adressetud.RootElement.StretchVertically = true;
            this.adressetud.Size = new System.Drawing.Size(135, 22);
            this.adressetud.TabIndex = 123;
            this.adressetud.TabStop = false;
            this.adressetud.Leave += new System.EventHandler(this.leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label21.ForeColor = System.Drawing.Color.Teal;
            this.label21.Location = new System.Drawing.Point(218, 282);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(123, 21);
            this.label21.TabIndex = 122;
            this.label21.Text = "Adresse Actuale:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Teal;
            this.pictureBox3.ErrorImage = null;
            this.pictureBox3.Image = global::etablissmentt.Properties.Resources.cam;
            this.pictureBox3.Location = new System.Drawing.Point(122, 110);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(20, 18);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 125;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.pic);
            this.pictureBox3.MouseEnter += new System.EventHandler(this.enter);
            this.pictureBox3.MouseLeave += new System.EventHandler(this.lev);
            // 
            // lab
            // 
            this.lab.AutoSize = true;
            this.lab.BackColor = System.Drawing.Color.Teal;
            this.lab.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lab.Location = new System.Drawing.Point(139, 111);
            this.lab.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab.Name = "lab";
            this.lab.Size = new System.Drawing.Size(55, 17);
            this.lab.TabIndex = 126;
            this.lab.Text = "modfier";
            this.lab.Visible = false;
            this.lab.Click += new System.EventHandler(this.pic);
            this.lab.MouseEnter += new System.EventHandler(this.enter);
            this.lab.MouseLeave += new System.EventHandler(this.lev);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox8.Image = global::etablissmentt.Properties.Resources.Professions_Teacher_512;
            this.pictureBox8.Location = new System.Drawing.Point(122, 27);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(98, 102);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 124;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pic);
            this.pictureBox8.MouseEnter += new System.EventHandler(this.enter);
            this.pictureBox8.MouseLeave += new System.EventHandler(this.lev);
            // 
            // gridetud
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.gridetud.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridetud.AutoGenerateColumns = false;
            this.gridetud.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridetud.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridetud.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridetud.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSeaGreen;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridetud.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridetud.ColumnHeadersHeight = 42;
            this.gridetud.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idProfDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.prenomDataGridViewTextBoxColumn,
            this.adresseDataGridViewTextBoxColumn,
            this.gradeDataGridViewTextBoxColumn,
            this.mailDataGridViewTextBoxColumn,
            this.dateDeNaissanceDataGridViewTextBoxColumn,
            this.lieuDeNaissanceDataGridViewTextBoxColumn,
            this.genderDataGridViewTextBoxColumn,
            this.photo});
            this.gridetud.DataSource = this.professeurBindingSource;
            this.gridetud.DoubleBuffered = true;
            this.gridetud.EnableHeadersVisualStyles = false;
            this.gridetud.GridColor = System.Drawing.Color.Teal;
            this.gridetud.HeaderBgColor = System.Drawing.Color.Teal;
            this.gridetud.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridetud.Location = new System.Drawing.Point(377, 79);
            this.gridetud.Margin = new System.Windows.Forms.Padding(2);
            this.gridetud.Name = "gridetud";
            this.gridetud.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridetud.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridetud.RowHeadersWidth = 20;
            this.gridetud.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gridetud.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Teal;
            this.gridetud.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Teal;
            this.gridetud.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridetud.RowTemplate.Height = 42;
            this.gridetud.RowTemplate.ReadOnly = true;
            this.gridetud.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridetud.Size = new System.Drawing.Size(488, 332);
            this.gridetud.TabIndex = 127;
            this.gridetud.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridetud_CellContentClick);
            this.gridetud.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.selec);
            // 
            // professeurTableAdapter
            // 
            this.professeurTableAdapter.ClearBeforeFill = true;
            // 
            // impbtn
            // 
            this.impbtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.impbtn.BackColor = System.Drawing.Color.Teal;
            this.impbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.impbtn.BorderRadius = 0;
            this.impbtn.ButtonText = "imprimer la fiche de poste";
            this.impbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.impbtn.DisabledColor = System.Drawing.Color.Gray;
            this.impbtn.Iconcolor = System.Drawing.Color.Transparent;
            this.impbtn.Iconimage = ((System.Drawing.Image)(resources.GetObject("impbtn.Iconimage")));
            this.impbtn.Iconimage_right = null;
            this.impbtn.Iconimage_right_Selected = null;
            this.impbtn.Iconimage_Selected = null;
            this.impbtn.IconMarginLeft = 0;
            this.impbtn.IconMarginRight = 0;
            this.impbtn.IconRightVisible = true;
            this.impbtn.IconRightZoom = 0D;
            this.impbtn.IconVisible = true;
            this.impbtn.IconZoom = 55D;
            this.impbtn.IsTab = false;
            this.impbtn.Location = new System.Drawing.Point(3, 63);
            this.impbtn.Name = "impbtn";
            this.impbtn.Normalcolor = System.Drawing.Color.Teal;
            this.impbtn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.impbtn.OnHoverTextColor = System.Drawing.Color.White;
            this.impbtn.selected = false;
            this.impbtn.Size = new System.Drawing.Size(119, 42);
            this.impbtn.TabIndex = 128;
            this.impbtn.Text = "imprimer la fiche de poste";
            this.impbtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.impbtn.Textcolor = System.Drawing.Color.White;
            this.impbtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.impbtn.Click += new System.EventHandler(this.impbtn_Click);
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            this.report1.RegisterData(this.ecoledeformationDataSet, "ecoledeformationDataSet");
            // 
            // idProfDataGridViewTextBoxColumn
            // 
            this.idProfDataGridViewTextBoxColumn.DataPropertyName = "IdProf";
            this.idProfDataGridViewTextBoxColumn.HeaderText = "IdProf";
            this.idProfDataGridViewTextBoxColumn.Name = "idProfDataGridViewTextBoxColumn";
            this.idProfDataGridViewTextBoxColumn.Visible = false;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "Nom";
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            // 
            // prenomDataGridViewTextBoxColumn
            // 
            this.prenomDataGridViewTextBoxColumn.DataPropertyName = "Prenom";
            this.prenomDataGridViewTextBoxColumn.HeaderText = "Prénom";
            this.prenomDataGridViewTextBoxColumn.Name = "prenomDataGridViewTextBoxColumn";
            // 
            // adresseDataGridViewTextBoxColumn
            // 
            this.adresseDataGridViewTextBoxColumn.DataPropertyName = "Adresse";
            this.adresseDataGridViewTextBoxColumn.HeaderText = "Adresse";
            this.adresseDataGridViewTextBoxColumn.Name = "adresseDataGridViewTextBoxColumn";
            // 
            // gradeDataGridViewTextBoxColumn
            // 
            this.gradeDataGridViewTextBoxColumn.DataPropertyName = "Grade";
            this.gradeDataGridViewTextBoxColumn.HeaderText = "Grade";
            this.gradeDataGridViewTextBoxColumn.Name = "gradeDataGridViewTextBoxColumn";
            // 
            // mailDataGridViewTextBoxColumn
            // 
            this.mailDataGridViewTextBoxColumn.DataPropertyName = "Mail";
            this.mailDataGridViewTextBoxColumn.HeaderText = "Mail";
            this.mailDataGridViewTextBoxColumn.Name = "mailDataGridViewTextBoxColumn";
            // 
            // dateDeNaissanceDataGridViewTextBoxColumn
            // 
            this.dateDeNaissanceDataGridViewTextBoxColumn.DataPropertyName = "Date de naissance";
            this.dateDeNaissanceDataGridViewTextBoxColumn.HeaderText = "Date de naissance";
            this.dateDeNaissanceDataGridViewTextBoxColumn.Name = "dateDeNaissanceDataGridViewTextBoxColumn";
            // 
            // lieuDeNaissanceDataGridViewTextBoxColumn
            // 
            this.lieuDeNaissanceDataGridViewTextBoxColumn.DataPropertyName = "Lieu de naissance";
            this.lieuDeNaissanceDataGridViewTextBoxColumn.HeaderText = "Lieu de naissance";
            this.lieuDeNaissanceDataGridViewTextBoxColumn.Name = "lieuDeNaissanceDataGridViewTextBoxColumn";
            // 
            // genderDataGridViewTextBoxColumn
            // 
            this.genderDataGridViewTextBoxColumn.DataPropertyName = "gender";
            this.genderDataGridViewTextBoxColumn.HeaderText = "Genre";
            this.genderDataGridViewTextBoxColumn.Name = "genderDataGridViewTextBoxColumn";
            // 
            // photo
            // 
            this.photo.DataPropertyName = "photo";
            this.photo.HeaderText = "photo";
            this.photo.Name = "photo";
            // 
            // divisionProf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.impbtn);
            this.Controls.Add(this.gridetud);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lab);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.adressetud);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grad);
            this.Controls.Add(this.emailetud);
            this.Controls.Add(this.lieunaissetud);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.dateetud);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.prenometud);
            this.Controls.Add(this.nometud);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "divisionProf";
            this.Size = new System.Drawing.Size(879, 477);
            this.Load += new System.EventHandler(this.divisionProf_Load);
            ((System.ComponentModel.ISupportInitialize)(this.prenometud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.professeurBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nometud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lieunaissetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grad)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adressetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

            }
            private Telerik.WinControls.UI.RadTextBox prenometud;
            private Telerik.WinControls.UI.RadTextBox nometud;
            private System.Windows.Forms.Label label4;
            private System.Windows.Forms.Label label3;
            private Telerik.WinControls.UI.RadDateTimePicker dateetud;
            private System.Windows.Forms.Label label7;
            private System.Windows.Forms.Label label5;
            private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
            private System.Windows.Forms.RadioButton radioButton2;
            private System.Windows.Forms.RadioButton radioButton1;
            private System.Windows.Forms.Label label1;
            private System.Windows.Forms.Label label2;
            private Telerik.WinControls.UI.RadTextBox lieunaissetud;
            private Telerik.WinControls.UI.RadTextBox emailetud;
            private Telerik.WinControls.UI.RadTextBox grad;
            private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.BindingSource professeurBindingSource;
        private ecoledeformationDataSet ecoledeformationDataSet;
        private ecoledeformationDataSetTableAdapters.professeurTableAdapter professeurTableAdapter;
        private Telerik.WinControls.UI.RadTextBox adressetud;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lab;
        private System.Windows.Forms.PictureBox pictureBox8;
        private Bunifu.Framework.UI.BunifuCustomDataGrid gridetud;
        private Bunifu.Framework.UI.BunifuFlatButton impbtn;
        private FastReport.Report report1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProfDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adresseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gradeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDeNaissanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lieuDeNaissanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn genderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn photo;
    }
    }
