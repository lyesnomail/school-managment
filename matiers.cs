﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace etablissmentt
{
    public partial class matiers : UserControl
    {
        public matiers()
        {
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void radGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void radTextBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void matiers_Load(object sender, EventArgs e)
        {
            examTableAdapter.Fill(ecoledeformationDataSet.exam);
            matiereTableAdapter.Fill(ecoledeformationDataSet.matiere,"%%");
            this.nivscolTableAdapter.Fill(this.ecoledeformationDataSet.nivscol);
            // TODO: cette ligne de code charge les données dans la table 'ecoledeformationDataSet.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.classeTableAdapter.Fill(this.ecoledeformationDataSet.classe);
            // TODO: cette ligne de code charge les données dans la table 'ecoledeformationDataSet.annscoll'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.annscollTableAdapter.Fill(this.ecoledeformationDataSet.annscoll);
        }

        private void prenometud_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void radDropDownList1_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void nometud_TextChanged(object sender, EventArgs e)
        {

        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {

        }

        private void radDropDownList1_SelectedIndexChanged_1(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

        }

        private void nivscolmatToolStripButton_Click(object sender, EventArgs e)
        {


        }

        private void bunifuCustomDataGrid1_SelectionChanged(object sender, EventArgs e)
        {
          
        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void f(object sender, EventArgs e)
        {

        }

        private void ajoutmat(object sender, EventArgs e)
        {
            if (label10.Text == "Ajouter")
            {
                matiereBindingSource.AddNew();
                matiereBindingSource.ResetCurrentItem();
                label10.Text = "Confirmer";
            }


            else if (label10.Text == "Confirmer")
            {
               
                if (nombox.Text == "") { MessageBox.Show("vous devez remplir le nom de la mtiere"); nombox.Focus(); }
                else if (coefbox.Text == "") { MessageBox.Show("vous devez remplir l coeftion"); coefbox.Focus(); }
                else if (coefbox.Text == "") { MessageBox.Show("vous devez remplir le niveau scolaire"); coefbox.Focus(); }
                else
                {


                    label10.Text = "Ajouter";
                }
                matiereBindingSource.EndEdit();
                matiereTableAdapter.Update(ecoledeformationDataSet.matiere);
                matiereTableAdapter.Fill(ecoledeformationDataSet.matiere, "%%");
            }
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void supp(object sender, EventArgs e)
        {
            if (gridmat.Rows.Count > 0)
            {
                DialogResult d = MessageBox.Show("vous voulez vraiment supprimer la matiere?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (d.ToString().CompareTo("OK") == 0)
                {
                    int id = int.Parse(gridmat.SelectedRows[0].Cells[0].Value.ToString());

                    matiereTableAdapter.Delete(id);
                    matiereTableAdapter.Fill(ecoledeformationDataSet.matiere,"%%");
                }
            }
        }

        private void modifymat(object sender, EventArgs e)
        {
            if (nombox.Text == "") { MessageBox.Show("vous devez remplir le nom de la mtiere"); nombox.Focus(); }
            else if (coefbox.Text == "") { MessageBox.Show("vous devez remplir l coeftion"); coefbox.Focus(); }
            else if (nvscol.Text == "") { MessageBox.Show("vous devez remplir le niveau scolaire"); nvscol.Focus(); }
            else
            {
                matiereBindingSource.EndEdit();
                matiereTableAdapter.Update(ecoledeformationDataSet.matiere);
                matiereTableAdapter.Fill(ecoledeformationDataSet.matiere,"%%");
            }
        }

        private void radTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void ajouexam(object sender, EventArgs e)
        {
            if (label11.Text == "ajouter")
            {

                examBindingSource.AddNew();
                examBindingSource.ResetCurrentItem();
                label11.Text = "confirmer";
            }

            else if (label11.Text == "Confirmer")
            {
                if (nomexam.Text == "") { MessageBox.Show("vous devez remplir le nom de l'examain"); nomexam.Focus(); }
                else if (typeexm.Text == "") { MessageBox.Show("vous devez remplir le type d'exmain"); typeexm.Focus(); }
                else if (mat.Text == "") { MessageBox.Show("vous devez remplir le trimestre"); mat.Focus(); }
                else
                {
                    examBindingSource.EndEdit();
                    examTableAdapter.Update(ecoledeformationDataSet.exam);
                    examTableAdapter.Fill(ecoledeformationDataSet.exam);



                }

                label11.Text = "Ajouter";
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void modifyexm(object sender, EventArgs e)
        {
            if (nomexam.Text == "") { MessageBox.Show("vous devez remplir le nom de l'examain"); nomexam.Focus(); }
            else if (typeexm.Text == "") { MessageBox.Show("vous devez remplir le type d'exmain"); typeexm.Focus(); }
            else if (mat.Text == "") { MessageBox.Show("vous devez remplir la trimestre"); mat.Focus(); }
            else
            {
                examBindingSource.EndEdit();
                examTableAdapter.Update(ecoledeformationDataSet.exam);
                examTableAdapter.Fill(ecoledeformationDataSet.exam);
            }
        }

        private void suppersio(object sender, EventArgs e)
        {

            if (gridTotal.Rows.Count > 0)
            {
                DialogResult d = MessageBox.Show("vous voulez vraiment supprimer l'examen?s", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (d.ToString().CompareTo("OK") == 0)
                {
                    int id = int.Parse(gridTotal.SelectedRows[0].Cells[0].Value.ToString());
                    examTableAdapter.Delete(id);
                    examTableAdapter.Fill(ecoledeformationDataSet.exam);
                }

            }
        }

        private void rech(object sender, EventArgs e)
        {
            string t = nvscol.ValueMember;
            matiereTableAdapter.Fill(ecoledeformationDataSet.matiere,"%%");

        }

        private void actulisgridmat(object sender, EventArgs e)
        {
            matiereTableAdapter.Fill(ecoledeformationDataSet.matiere,"%%");
        }

        private void actuexam(object sender, EventArgs e)
        {
            matiereTableAdapter.Fill(ecoledeformationDataSet.matiere, "%%");
        }
    }
}
