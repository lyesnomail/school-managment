﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace etablissmentt
{
    public partial class divisionProf : UserControl
    {
        public divisionProf()
        {
            InitializeComponent();
        }
        bool IsValidEmail(string text)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(text);
                return addr.Address == text;

            }
            catch { return false; }
        }

        private void divisionProf_Load(object sender, EventArgs e)
        {
            this.professeurTableAdapter.Fill(this.ecoledeformationDataSet.professeur);
            int id = Int32.Parse(this.professeurTableAdapter.minid().ToString());

            byte[] img = this.professeurTableAdapter.img2(id);
            if (img != null)
            { MemoryStream ms = new MemoryStream(img); pictureBox2.Image = Image.FromStream(ms); }
            string ge = professeurTableAdapter.gend(id);
            if (ge != null)
            {
                if (ge == "Garçon") radioButton1.Checked = true;
                else radioButton2.Checked = true;

            }
            for (int i = 0; i < gridetud.Columns.Count; i++)
                if (gridetud.Columns[i] is DataGridViewImageColumn)
                {
                    ((DataGridViewImageColumn)gridetud.Columns[i]).ImageLayout = DataGridViewImageCellLayout.Stretch;

                    break;
                }
        }

        private void radDropDownList1_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

        }

        private void typeexm_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void radDropDownList2_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

        }

        private void radTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void x(object sender, EventArgs e)
        {
            if (label11.Text == "Ajouter")
            {
                label11.Text = "Confirmer";

                professeurBindingSource.AddNew();
                professeurBindingSource.ResetCurrentItem();
            }
            else
            {
               
                if (nometud.Text == "") { MessageBox.Show("vous devez remplir le nom"); nometud.Focus(); }
                else if (prenometud.Text == "") { MessageBox.Show("vous devez remplir le nom"); prenometud.Focus(); }
                else if (dateetud.Text == "") { MessageBox.Show("vous devez remplir la date de naiscance"); dateetud.Focus(); }
                else if (lieunaissetud.Text == "") { MessageBox.Show("vous devez remplir le lieu de naissnace"); lieunaissetud.Focus(); }
                else if (adressetud.Text == "") { MessageBox.Show("vous devez remplir l'adresse"); adressetud.Focus(); }
                else if (emailetud.Text == "" || !IsValidEmail(emailetud.Text)) { MessageBox.Show("vous devez remplir une adress email juste"); emailetud.Focus(); }
                else if (grad.Text == "" || !IsValidEmail(emailetud.Text)) { MessageBox.Show("vous devez remplir le grade"); grad.Focus(); }
                else
                {
              

                    
                    MemoryStream ms = new MemoryStream();
                    pictureBox2.Image.Save(ms, pictureBox2.Image.RawFormat);
                    byte[] img = ms.ToArray();

                    //professeurTableAdapter.imgin( img, radioButtonGroup.SelectedValue.ToString(), id);
                  professeurBindingSource.EndEdit();
                    this.professeurTableAdapter.Fill(this.ecoledeformationDataSet.professeur);
                    label11.Text = "Ajouter";
                    professeurBindingSource.EndEdit();
                    professeurTableAdapter.Update(ecoledeformationDataSet);

                }
            }
        }

        private void f(object sender, EventArgs e)
        {
         
            
           
                professeurBindingSource.EndEdit();
               professeurTableAdapter.Update(ecoledeformationDataSet);

                if (nometud.Text == "") { MessageBox.Show("vous devez remplir le nom"); nometud.Focus(); }
                else if (prenometud.Text == "") { MessageBox.Show("vous devez remplir le nom"); prenometud.Focus(); }
                else if (dateetud.Text == "") { MessageBox.Show("vous devez remplir la date de naiscance"); dateetud.Focus(); }
                else if (lieunaissetud.Text == "") { MessageBox.Show("vous devez remplir le lieu de naissnace"); lieunaissetud.Focus(); }
                else if (adressetud.Text == "") { MessageBox.Show("vous devez remplir l'adresse"); adressetud.Focus(); }
                else if (emailetud.Text == "" || !IsValidEmail(emailetud.Text)) { MessageBox.Show("vous devez remplir une adress email juste"); emailetud.Focus(); }
                else if (grad.Text == "" || !IsValidEmail(emailetud.Text)) { MessageBox.Show("vous devez remplir le grade"); grad.Focus(); }
                else
                { MemoryStream ms = new MemoryStream();
                    pictureBox2.Image.Save(ms, pictureBox2.Image.RawFormat);
                    byte[] img = ms.ToArray();
                    int id = int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString());
                    string v;
                    if (radioButton1.Checked)
                    v=radioButton1.Text;
                    else   v=radioButton2.Text;
                    professeurTableAdapter.min(v,img, id);
                    professeurBindingSource.EndEdit();
                    this.professeurTableAdapter.Fill(this.ecoledeformationDataSet.professeur);
                }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void b(object sender, EventArgs e)
        {
            if (gridetud.Rows.Count > 0)
            {
                DialogResult d = MessageBox.Show("vous voulez vraiment supprimer le profissuer?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (d.ToString().CompareTo("OK") == 0)
                {
                    int id = int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString());
                    professeurTableAdapter.Delete(id);
                    professeurTableAdapter.Fill(ecoledeformationDataSet.professeur);
                }

            }
        }

        private void pic(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Choose Image(*.jpg; *.png; *.gif)|*.jpg; *.png; *.gif";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.Image = Image.FromFile(opf.FileName);
            }
        }

        private void enter(object sender, EventArgs e)
        {
            pictureBox3.Visible = true;
            lab.Visible = true;
        }

        private void lev(object sender, EventArgs e)
        {
            pictureBox3.Visible = false; lab.Visible = false;
        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {

        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void click(object sender, EventArgs e)
        {
            this.professeurTableAdapter.Fill(this.ecoledeformationDataSet.professeur);
        }

        private void leave(object sender, EventArgs e)
        {

        }

        private void selec(object sender, DataGridViewCellMouseEventArgs e)
        {
            byte[] img = this.professeurTableAdapter.img2(int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString()));

            if (img != null)
            {
                MemoryStream ms = new MemoryStream(img);
                pictureBox2.Image = Image.FromStream(ms);
            }
            else pictureBox2.Image = etablissmentt.Properties.Resources.t;
            string ge = professeurTableAdapter.gend(int.Parse(gridetud.SelectedRows[0].Cells[0].Value.ToString()));
            if (ge != null)
            {
                if (ge == "Garçon") radioButton1.Checked = true;
                else radioButton2.Checked = true;

            }
        }

        private void impbtn_Click(object sender, EventArgs e)
        {
            if (gridetud.Rows.Count > 0) 
            {
                int tmp_id_professeur = int.Parse(gridetud.SelectedRows[0].Cells["IdProf"].Value.ToString());

                professeurTableAdapter.Fill(ecoledeformationDataSet.professeur);
                report1.Show();
            }
        }

        private void gridetud_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
