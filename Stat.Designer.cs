﻿namespace etablissmentt
{
    partial class Stat
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Stat));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.abssanceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ecoledeformationDataSet = new etablissmentt.ecoledeformationDataSet();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dtp_fin = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.dtp_debut = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.cmb_etudiant = new Bunifu.UI.WinForms.BunifuDropdown();
            this.etudiantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmb_matiere = new Bunifu.UI.WinForms.BunifuDropdown();
            this.matiereBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.cmb_examen = new Bunifu.UI.WinForms.BunifuDropdown();
            this.examBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.chart_nb_et_cl = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.etudierBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmb_nv_scolaire = new Bunifu.UI.WinForms.BunifuDropdown();
            this.nivscolBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.abssanceTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.abssanceTableAdapter();
            this.etudiantTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.etudiantTableAdapter();
            this.classeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.classeTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.classeTableAdapter();
            this.matiereTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.matiereTableAdapter();
            this.examTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.examTableAdapter();
            this.nivscolTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.nivscolTableAdapter();
            this.etudierTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.etudierTableAdapter();
            this.bsearch = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abssanceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matiereBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_nb_et_cl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudierBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nivscolBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 13;
            this.bunifuElipse1.TargetControl = this;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(879, 477);
            this.tabControl1.TabIndex = 56;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chart1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(871, 451);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Absence";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chart1.BorderlineColor = System.Drawing.Color.WhiteSmoke;
            chartArea4.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea4);
            this.chart1.DataSource = this.abssanceBindingSource;
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend4.Name = "Legend1";
            this.chart1.Legends.Add(legend4);
            this.chart1.Location = new System.Drawing.Point(3, 97);
            this.chart1.Margin = new System.Windows.Forms.Padding(2);
            this.chart1.Name = "chart1";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Absances";
            series4.XValueMember = "Etudiant";
            series4.YValueMembers = "nbrHeur";
            this.chart1.Series.Add(series4);
            this.chart1.Size = new System.Drawing.Size(865, 351);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            // 
            // abssanceBindingSource
            // 
            this.abssanceBindingSource.DataMember = "abssance";
            this.abssanceBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // ecoledeformationDataSet
            // 
            this.ecoledeformationDataSet.DataSetName = "ecoledeformationDataSet";
            this.ecoledeformationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.bsearch);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dtp_fin);
            this.groupBox1.Controls.Add(this.dtp_debut);
            this.groupBox1.Controls.Add(this.cmb_etudiant);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(865, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rechercher";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(387, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Date Fin :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(369, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Date Début  :";
            // 
            // dtp_fin
            // 
            this.dtp_fin.BorderRadius = 1;
            this.dtp_fin.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thin;
            this.dtp_fin.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.dtp_fin.DisabledColor = System.Drawing.Color.Gray;
            this.dtp_fin.DisplayWeekNumbers = false;
            this.dtp_fin.DPHeight = 0;
            this.dtp_fin.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtp_fin.FillDatePicker = false;
            this.dtp_fin.ForeColor = System.Drawing.Color.Purple;
            this.dtp_fin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_fin.Icon = ((System.Drawing.Image)(resources.GetObject("dtp_fin.Icon")));
            this.dtp_fin.IconColor = System.Drawing.Color.Purple;
            this.dtp_fin.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.dtp_fin.Location = new System.Drawing.Point(446, 50);
            this.dtp_fin.MinimumSize = new System.Drawing.Size(217, 25);
            this.dtp_fin.Name = "dtp_fin";
            this.dtp_fin.Size = new System.Drawing.Size(217, 32);
            this.dtp_fin.TabIndex = 3;
            // 
            // dtp_debut
            // 
            this.dtp_debut.BorderRadius = 1;
            this.dtp_debut.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thin;
            this.dtp_debut.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.dtp_debut.DisabledColor = System.Drawing.Color.Gray;
            this.dtp_debut.DisplayWeekNumbers = false;
            this.dtp_debut.DPHeight = 0;
            this.dtp_debut.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtp_debut.FillDatePicker = false;
            this.dtp_debut.ForeColor = System.Drawing.Color.Purple;
            this.dtp_debut.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_debut.Icon = ((System.Drawing.Image)(resources.GetObject("dtp_debut.Icon")));
            this.dtp_debut.IconColor = System.Drawing.Color.Purple;
            this.dtp_debut.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.dtp_debut.Location = new System.Drawing.Point(446, 12);
            this.dtp_debut.MinimumSize = new System.Drawing.Size(217, 32);
            this.dtp_debut.Name = "dtp_debut";
            this.dtp_debut.Size = new System.Drawing.Size(217, 32);
            this.dtp_debut.TabIndex = 2;
            // 
            // cmb_etudiant
            // 
            this.cmb_etudiant.BackColor = System.Drawing.Color.Transparent;
            this.cmb_etudiant.BorderRadius = 1;
            this.cmb_etudiant.Color = System.Drawing.Color.Gray;
            this.cmb_etudiant.DataSource = this.etudiantBindingSource;
            this.cmb_etudiant.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down;
            this.cmb_etudiant.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_etudiant.DisplayMember = "Etudiant";
            this.cmb_etudiant.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_etudiant.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thick;
            this.cmb_etudiant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_etudiant.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left;
            this.cmb_etudiant.FillDropDown = false;
            this.cmb_etudiant.FillIndicator = false;
            this.cmb_etudiant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmb_etudiant.ForeColor = System.Drawing.Color.Purple;
            this.cmb_etudiant.FormattingEnabled = true;
            this.cmb_etudiant.Icon = null;
            this.cmb_etudiant.IndicatorColor = System.Drawing.Color.Purple;
            this.cmb_etudiant.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right;
            this.cmb_etudiant.ItemBackColor = System.Drawing.Color.White;
            this.cmb_etudiant.ItemBorderColor = System.Drawing.Color.White;
            this.cmb_etudiant.ItemForeColor = System.Drawing.Color.Purple;
            this.cmb_etudiant.ItemHeight = 26;
            this.cmb_etudiant.ItemHighLightColor = System.Drawing.Color.Thistle;
            this.cmb_etudiant.Location = new System.Drawing.Point(68, 30);
            this.cmb_etudiant.Name = "cmb_etudiant";
            this.cmb_etudiant.Size = new System.Drawing.Size(269, 32);
            this.cmb_etudiant.TabIndex = 1;
            this.cmb_etudiant.Text = null;
            this.cmb_etudiant.ValueMember = "IdEtud";
            this.cmb_etudiant.SelectedIndexChanged += new System.EventHandler(this.cmb_etudiant_SelectedIndexChanged);
            // 
            // etudiantBindingSource
            // 
            this.etudiantBindingSource.DataMember = "etudiant";
            this.etudiantBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Etudiant :";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.chart2);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(871, 451);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Controle";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // chart2
            // 
            this.chart2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chart2.BorderlineColor = System.Drawing.Color.WhiteSmoke;
            chartArea5.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea5);
            this.chart2.Dock = System.Windows.Forms.DockStyle.Fill;
            legend5.Name = "Legend1";
            this.chart2.Legends.Add(legend5);
            this.chart2.Location = new System.Drawing.Point(3, 80);
            this.chart2.Margin = new System.Windows.Forms.Padding(2);
            this.chart2.Name = "chart2";
            this.chart2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Berry;
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Note";
            this.chart2.Series.Add(series5);
            this.chart2.Size = new System.Drawing.Size(865, 368);
            this.chart2.TabIndex = 55;
            this.chart2.Text = "chart2";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cmb_matiere);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmb_examen);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(865, 77);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rechercher";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Matière :";
            // 
            // cmb_matiere
            // 
            this.cmb_matiere.BackColor = System.Drawing.Color.Transparent;
            this.cmb_matiere.BorderRadius = 1;
            this.cmb_matiere.DataSource = this.matiereBindingSource;
            this.cmb_matiere.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down;
            this.cmb_matiere.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_matiere.DisplayMember = "nomMat";
            this.cmb_matiere.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_matiere.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thick;
            this.cmb_matiere.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_matiere.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left;
            this.cmb_matiere.FillDropDown = false;
            this.cmb_matiere.FillIndicator = false;
            this.cmb_matiere.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmb_matiere.ForeColor = System.Drawing.Color.Purple;
            this.cmb_matiere.FormattingEnabled = true;
            this.cmb_matiere.Icon = null;
            this.cmb_matiere.IndicatorColor = System.Drawing.Color.Purple;
            this.cmb_matiere.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right;
            this.cmb_matiere.ItemBackColor = System.Drawing.Color.White;
            this.cmb_matiere.ItemBorderColor = System.Drawing.Color.White;
            this.cmb_matiere.ItemForeColor = System.Drawing.Color.Purple;
            this.cmb_matiere.ItemHeight = 26;
            this.cmb_matiere.ItemHighLightColor = System.Drawing.Color.Thistle;
            this.cmb_matiere.Location = new System.Drawing.Point(58, 23);
            this.cmb_matiere.Name = "cmb_matiere";
            this.cmb_matiere.Size = new System.Drawing.Size(199, 32);
            this.cmb_matiere.TabIndex = 2;
            this.cmb_matiere.Text = null;
            this.cmb_matiere.ValueMember = "idmat";
            // 
            // matiereBindingSource
            // 
            this.matiereBindingSource.DataMember = "matiere";
            this.matiereBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Examen :";
            // 
            // cmb_examen
            // 
            this.cmb_examen.BackColor = System.Drawing.Color.Transparent;
            this.cmb_examen.BorderRadius = 1;
            this.cmb_examen.DataSource = this.examBindingSource;
            this.cmb_examen.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down;
            this.cmb_examen.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_examen.DisplayMember = "nomexam";
            this.cmb_examen.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_examen.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thick;
            this.cmb_examen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_examen.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left;
            this.cmb_examen.FillDropDown = false;
            this.cmb_examen.FillIndicator = false;
            this.cmb_examen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmb_examen.ForeColor = System.Drawing.Color.Purple;
            this.cmb_examen.FormattingEnabled = true;
            this.cmb_examen.Icon = null;
            this.cmb_examen.IndicatorColor = System.Drawing.Color.Purple;
            this.cmb_examen.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right;
            this.cmb_examen.ItemBackColor = System.Drawing.Color.White;
            this.cmb_examen.ItemBorderColor = System.Drawing.Color.White;
            this.cmb_examen.ItemForeColor = System.Drawing.Color.Purple;
            this.cmb_examen.ItemHeight = 26;
            this.cmb_examen.ItemHighLightColor = System.Drawing.Color.Thistle;
            this.cmb_examen.Location = new System.Drawing.Point(324, 23);
            this.cmb_examen.Name = "cmb_examen";
            this.cmb_examen.Size = new System.Drawing.Size(199, 32);
            this.cmb_examen.TabIndex = 0;
            this.cmb_examen.Text = null;
            this.cmb_examen.ValueMember = "idexam";
            // 
            // examBindingSource
            // 
            this.examBindingSource.DataMember = "exam";
            this.examBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.chart_nb_et_cl);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(871, 451);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Nombre";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // chart_nb_et_cl
            // 
            this.chart_nb_et_cl.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chart_nb_et_cl.BorderlineColor = System.Drawing.Color.WhiteSmoke;
            chartArea6.Name = "ChartArea1";
            this.chart_nb_et_cl.ChartAreas.Add(chartArea6);
            this.chart_nb_et_cl.DataSource = this.etudierBindingSource;
            this.chart_nb_et_cl.Dock = System.Windows.Forms.DockStyle.Fill;
            legend6.Name = "Legend1";
            this.chart_nb_et_cl.Legends.Add(legend6);
            this.chart_nb_et_cl.Location = new System.Drawing.Point(3, 101);
            this.chart_nb_et_cl.Margin = new System.Windows.Forms.Padding(2);
            this.chart_nb_et_cl.Name = "chart_nb_et_cl";
            this.chart_nb_et_cl.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            series6.XValueMember = "nomclass";
            this.chart_nb_et_cl.Series.Add(series6);
            this.chart_nb_et_cl.Size = new System.Drawing.Size(865, 347);
            this.chart_nb_et_cl.TabIndex = 57;
            this.chart_nb_et_cl.Text = "chart3";
            // 
            // etudierBindingSource
            // 
            this.etudierBindingSource.DataMember = "etudier";
            this.etudierBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.cmb_nv_scolaire);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(865, 98);
            this.groupBox3.TabIndex = 56;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rechercher";
            // 
            // cmb_nv_scolaire
            // 
            this.cmb_nv_scolaire.BackColor = System.Drawing.Color.Transparent;
            this.cmb_nv_scolaire.BorderRadius = 1;
            this.cmb_nv_scolaire.DataSource = this.nivscolBindingSource;
            this.cmb_nv_scolaire.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down;
            this.cmb_nv_scolaire.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_nv_scolaire.DisplayMember = "nomNS";
            this.cmb_nv_scolaire.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_nv_scolaire.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thick;
            this.cmb_nv_scolaire.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_nv_scolaire.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left;
            this.cmb_nv_scolaire.FillDropDown = false;
            this.cmb_nv_scolaire.FillIndicator = false;
            this.cmb_nv_scolaire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmb_nv_scolaire.ForeColor = System.Drawing.Color.Purple;
            this.cmb_nv_scolaire.FormattingEnabled = true;
            this.cmb_nv_scolaire.Icon = null;
            this.cmb_nv_scolaire.IndicatorColor = System.Drawing.Color.Purple;
            this.cmb_nv_scolaire.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right;
            this.cmb_nv_scolaire.ItemBackColor = System.Drawing.Color.White;
            this.cmb_nv_scolaire.ItemBorderColor = System.Drawing.Color.White;
            this.cmb_nv_scolaire.ItemForeColor = System.Drawing.Color.Purple;
            this.cmb_nv_scolaire.ItemHeight = 26;
            this.cmb_nv_scolaire.ItemHighLightColor = System.Drawing.Color.Thistle;
            this.cmb_nv_scolaire.Location = new System.Drawing.Point(147, 34);
            this.cmb_nv_scolaire.Name = "cmb_nv_scolaire";
            this.cmb_nv_scolaire.Size = new System.Drawing.Size(292, 32);
            this.cmb_nv_scolaire.TabIndex = 3;
            this.cmb_nv_scolaire.Text = null;
            this.cmb_nv_scolaire.ValueMember = "idNiveauScolaire";
            this.cmb_nv_scolaire.SelectedIndexChanged += new System.EventHandler(this.cmb_nv_scolaire_SelectedIndexChanged);
            // 
            // nivscolBindingSource
            // 
            this.nivscolBindingSource.DataMember = "nivscol";
            this.nivscolBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Niveau Scolaire :";
            // 
            // abssanceTableAdapter
            // 
            this.abssanceTableAdapter.ClearBeforeFill = true;
            // 
            // etudiantTableAdapter
            // 
            this.etudiantTableAdapter.ClearBeforeFill = true;
            // 
            // classeBindingSource
            // 
            this.classeBindingSource.DataMember = "classe";
            this.classeBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // classeTableAdapter
            // 
            this.classeTableAdapter.ClearBeforeFill = true;
            // 
            // matiereTableAdapter
            // 
            this.matiereTableAdapter.ClearBeforeFill = true;
            // 
            // examTableAdapter
            // 
            this.examTableAdapter.ClearBeforeFill = true;
            // 
            // nivscolTableAdapter
            // 
            this.nivscolTableAdapter.ClearBeforeFill = true;
            // 
            // etudierTableAdapter
            // 
            this.etudierTableAdapter.ClearBeforeFill = true;
            // 
            // bsearch
            // 
            this.bsearch.BackColor = System.Drawing.Color.Transparent;
            this.bsearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bsearch.BackgroundImage")));
            this.bsearch.ButtonText = "Rechercher";
            this.bsearch.ButtonTextMarginLeft = 0;
            this.bsearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bsearch.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.bsearch.DisabledFillColor = System.Drawing.Color.Gray;
            this.bsearch.DisabledForecolor = System.Drawing.Color.White;
            this.bsearch.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bsearch.ForeColor = System.Drawing.Color.Black;
            this.bsearch.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bsearch.IconPadding = 10;
            this.bsearch.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bsearch.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.bsearch.IdleBorderRadius = 1;
            this.bsearch.IdleBorderThickness = 1;
            this.bsearch.IdleFillColor = System.Drawing.Color.White;
            this.bsearch.IdleIconLeftImage = null;
            this.bsearch.IdleIconRightImage = null;
            this.bsearch.Location = new System.Drawing.Point(743, 24);
            this.bsearch.Name = "bsearch";
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(166)))), ((int)(((byte)(221)))));
            stateProperties3.BorderRadius = 1;
            stateProperties3.BorderThickness = 1;
            stateProperties3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(166)))), ((int)(((byte)(221)))));
            stateProperties3.ForeColor = System.Drawing.Color.White;
            stateProperties3.IconLeftImage = null;
            stateProperties3.IconRightImage = null;
            this.bsearch.onHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties4.BorderRadius = 1;
            stateProperties4.BorderThickness = 1;
            stateProperties4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties4.ForeColor = System.Drawing.Color.White;
            stateProperties4.IconLeftImage = null;
            stateProperties4.IconRightImage = null;
            this.bsearch.OnPressedState = stateProperties4;
            this.bsearch.Size = new System.Drawing.Size(103, 45);
            this.bsearch.TabIndex = 6;
            this.bsearch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bsearch.Click += new System.EventHandler(this.bsearch_Click);
            // 
            // Stat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Stat";
            this.Size = new System.Drawing.Size(879, 477);
            this.Load += new System.EventHandler(this.Stat_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abssanceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matiereBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart_nb_et_cl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudierBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nivscolBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_nb_et_cl;
        private System.Windows.Forms.Label label5;
        private Bunifu.UI.WinForms.BunifuDropdown cmb_etudiant;
        private System.Windows.Forms.BindingSource etudiantBindingSource;
        private ecoledeformationDataSet ecoledeformationDataSet;
        private System.Windows.Forms.BindingSource abssanceBindingSource;
        private ecoledeformationDataSetTableAdapters.abssanceTableAdapter abssanceTableAdapter;
        private ecoledeformationDataSetTableAdapters.etudiantTableAdapter etudiantTableAdapter;
        private System.Windows.Forms.Label label7;
        private Bunifu.UI.WinForms.BunifuDropdown cmb_matiere;
        private System.Windows.Forms.Label label6;
        private Bunifu.UI.WinForms.BunifuDropdown cmb_examen;
        private System.Windows.Forms.BindingSource matiereBindingSource;
        private System.Windows.Forms.BindingSource examBindingSource;
        private System.Windows.Forms.BindingSource classeBindingSource;
        private ecoledeformationDataSetTableAdapters.classeTableAdapter classeTableAdapter;
        private ecoledeformationDataSetTableAdapters.matiereTableAdapter matiereTableAdapter;
        private ecoledeformationDataSetTableAdapters.examTableAdapter examTableAdapter;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private Bunifu.UI.WinForms.BunifuDatePicker dtp_fin;
        private Bunifu.UI.WinForms.BunifuDatePicker dtp_debut;
        private Bunifu.UI.WinForms.BunifuDropdown cmb_nv_scolaire;
        private System.Windows.Forms.BindingSource nivscolBindingSource;
        private System.Windows.Forms.Label label1;
        private ecoledeformationDataSetTableAdapters.nivscolTableAdapter nivscolTableAdapter;
        private System.Windows.Forms.BindingSource etudierBindingSource;
        private ecoledeformationDataSetTableAdapters.etudierTableAdapter etudierTableAdapter;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bsearch;
    }
}
