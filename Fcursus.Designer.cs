﻿namespace etablissmentt
{
    partial class Fcursus

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }



        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fcursus));
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            this.lb3 = new System.Windows.Forms.Label();
            this.lb2 = new System.Windows.Forms.Label();
            this.gridTotal = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.hide = new System.Windows.Forms.Label();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lb4 = new System.Windows.Forms.Label();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList3 = new Telerik.WinControls.UI.RadDropDownList();
            this.supprimer = new Bunifu.Framework.UI.BunifuFlatButton();
            this.modfier = new Bunifu.Framework.UI.BunifuFlatButton();
            this.ajouter = new Bunifu.Framework.UI.BunifuFlatButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lb3
            // 
            this.lb3.AutoSize = true;
            this.lb3.BackColor = System.Drawing.Color.Transparent;
            this.lb3.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb3.ForeColor = System.Drawing.Color.Teal;
            this.lb3.Location = new System.Drawing.Point(94, 141);
            this.lb3.Name = "lb3";
            this.lb3.Size = new System.Drawing.Size(87, 38);
            this.lb3.TabIndex = 71;
            this.lb3.Text = "class:";
            // 
            // lb2
            // 
            this.lb2.AutoSize = true;
            this.lb2.BackColor = System.Drawing.Color.Transparent;
            this.lb2.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb2.ForeColor = System.Drawing.Color.Teal;
            this.lb2.Location = new System.Drawing.Point(101, 53);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(208, 38);
            this.lb2.TabIndex = 70;
            this.lb2.Text = "année scollaire:";
            this.lb2.Click += new System.EventHandler(this.lb2_Click);
            // 
            // gridTotal
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridTotal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridTotal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridTotal.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridTotal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTotal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTotal.DoubleBuffered = true;
            this.gridTotal.EnableHeadersVisualStyles = false;
            this.gridTotal.HeaderBgColor = System.Drawing.Color.Teal;
            this.gridTotal.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridTotal.Location = new System.Drawing.Point(375, 74);
            this.gridTotal.Name = "gridTotal";
            this.gridTotal.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gridTotal.RowTemplate.Height = 24;
            this.gridTotal.Size = new System.Drawing.Size(429, 294);
            this.gridTotal.TabIndex = 76;
            this.gridTotal.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridTotal_CellContentClick);
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.Teal;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Teal;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(88, 447);
            this.bunifuGradientPanel1.TabIndex = 77;
            // 
            // hide
            // 
            this.hide.AutoSize = true;
            this.hide.BackColor = System.Drawing.Color.Transparent;
            this.hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hide.ForeColor = System.Drawing.Color.Teal;
            this.hide.Location = new System.Drawing.Point(725, 10);
            this.hide.Name = "hide";
            this.hide.Size = new System.Drawing.Size(34, 44);
            this.hide.TabIndex = 117;
            this.hide.Text = "-";
            // 
            // radLabel1
            // 
            this.radLabel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radLabel1.Font = new System.Drawing.Font("Gill Sans MT", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.Teal;
            this.radLabel1.Location = new System.Drawing.Point(766, 13);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(31, 33);
            this.radLabel1.TabIndex = 116;
            this.radLabel1.Text = "X";
            // 
            // lb4
            // 
            this.lb4.AutoSize = true;
            this.lb4.BackColor = System.Drawing.Color.Transparent;
            this.lb4.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb4.ForeColor = System.Drawing.Color.Teal;
            this.lb4.Location = new System.Drawing.Point(101, 229);
            this.lb4.Name = "lb4";
            this.lb4.Size = new System.Drawing.Size(214, 38);
            this.lb4.TabIndex = 118;
            this.lb4.Text = "niveau scollaire:";
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.BackColor = System.Drawing.Color.Teal;
            this.radDropDownList1.EnableKeyMap = true;
            radListDataItem1.Text = "controle";
            radListDataItem2.Text = "exmain finale";
            this.radDropDownList1.Items.Add(radListDataItem1);
            this.radDropDownList1.Items.Add(radListDataItem2);
            this.radDropDownList1.Location = new System.Drawing.Point(100, 102);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(251, 24);
            this.radDropDownList1.TabIndex = 121;
            this.radDropDownList1.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownList1_SelectedIndexChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList1.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList1.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList1.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList1.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            // 
            // radDropDownList2
            // 
            this.radDropDownList2.BackColor = System.Drawing.Color.Teal;
            this.radDropDownList2.EnableKeyMap = true;
            radListDataItem3.Text = "controle";
            radListDataItem4.Text = "exmain finale";
            this.radDropDownList2.Items.Add(radListDataItem3);
            this.radDropDownList2.Items.Add(radListDataItem4);
            this.radDropDownList2.Location = new System.Drawing.Point(101, 182);
            this.radDropDownList2.Name = "radDropDownList2";
            this.radDropDownList2.Size = new System.Drawing.Size(251, 24);
            this.radDropDownList2.TabIndex = 122;
            this.radDropDownList2.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownList2_SelectedIndexChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList2.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList2.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            // 
            // radDropDownList3
            // 
            this.radDropDownList3.BackColor = System.Drawing.Color.Teal;
            this.radDropDownList3.EnableKeyMap = true;
            radListDataItem5.Text = "controle";
            radListDataItem6.Text = "exmain finale";
            this.radDropDownList3.Items.Add(radListDataItem5);
            this.radDropDownList3.Items.Add(radListDataItem6);
            this.radDropDownList3.Location = new System.Drawing.Point(101, 283);
            this.radDropDownList3.Name = "radDropDownList3";
            this.radDropDownList3.Size = new System.Drawing.Size(251, 24);
            this.radDropDownList3.TabIndex = 123;
            this.radDropDownList3.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownList3_SelectedIndexChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList3.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList3.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList3.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList3.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            // 
            // supprimer
            // 
            this.supprimer.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.supprimer.BackColor = System.Drawing.Color.Teal;
            this.supprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.supprimer.BorderRadius = 0;
            this.supprimer.ButtonText = "Supprimer";
            this.supprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.supprimer.DisabledColor = System.Drawing.Color.Gray;
            this.supprimer.Iconcolor = System.Drawing.Color.Transparent;
            this.supprimer.Iconimage = ((System.Drawing.Image)(resources.GetObject("supprimer.Iconimage")));
            this.supprimer.Iconimage_right = null;
            this.supprimer.Iconimage_right_Selected = null;
            this.supprimer.Iconimage_Selected = null;
            this.supprimer.IconMarginLeft = 0;
            this.supprimer.IconMarginRight = 0;
            this.supprimer.IconRightVisible = true;
            this.supprimer.IconRightZoom = 0D;
            this.supprimer.IconVisible = true;
            this.supprimer.IconZoom = 61D;
            this.supprimer.IsTab = false;
            this.supprimer.Location = new System.Drawing.Point(377, 396);
            this.supprimer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.supprimer.Name = "supprimer";
            this.supprimer.Normalcolor = System.Drawing.Color.Teal;
            this.supprimer.OnHovercolor = System.Drawing.Color.Crimson;
            this.supprimer.OnHoverTextColor = System.Drawing.Color.White;
            this.supprimer.selected = false;
            this.supprimer.Size = new System.Drawing.Size(123, 31);
            this.supprimer.TabIndex = 126;
            this.supprimer.Text = "Supprimer";
            this.supprimer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.supprimer.Textcolor = System.Drawing.Color.White;
            this.supprimer.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // modfier
            // 
            this.modfier.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.modfier.BackColor = System.Drawing.Color.Teal;
            this.modfier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.modfier.BorderRadius = 0;
            this.modfier.ButtonText = "Modfier";
            this.modfier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modfier.DisabledColor = System.Drawing.Color.Gray;
            this.modfier.Iconcolor = System.Drawing.Color.Transparent;
            this.modfier.Iconimage = ((System.Drawing.Image)(resources.GetObject("modfier.Iconimage")));
            this.modfier.Iconimage_right = null;
            this.modfier.Iconimage_right_Selected = null;
            this.modfier.Iconimage_Selected = null;
            this.modfier.IconMarginLeft = 0;
            this.modfier.IconMarginRight = 0;
            this.modfier.IconRightVisible = true;
            this.modfier.IconRightZoom = 0D;
            this.modfier.IconVisible = true;
            this.modfier.IconZoom = 62D;
            this.modfier.IsTab = false;
            this.modfier.Location = new System.Drawing.Point(242, 396);
            this.modfier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.modfier.Name = "modfier";
            this.modfier.Normalcolor = System.Drawing.Color.Teal;
            this.modfier.OnHovercolor = System.Drawing.Color.Gold;
            this.modfier.OnHoverTextColor = System.Drawing.Color.White;
            this.modfier.selected = false;
            this.modfier.Size = new System.Drawing.Size(123, 31);
            this.modfier.TabIndex = 125;
            this.modfier.Text = "Modfier";
            this.modfier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.modfier.Textcolor = System.Drawing.Color.White;
            this.modfier.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // ajouter
            // 
            this.ajouter.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.ajouter.BackColor = System.Drawing.Color.Teal;
            this.ajouter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ajouter.BorderRadius = 0;
            this.ajouter.ButtonText = "Ajouter";
            this.ajouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ajouter.DisabledColor = System.Drawing.Color.Gray;
            this.ajouter.Iconcolor = System.Drawing.Color.Transparent;
            this.ajouter.Iconimage = ((System.Drawing.Image)(resources.GetObject("ajouter.Iconimage")));
            this.ajouter.Iconimage_right = null;
            this.ajouter.Iconimage_right_Selected = null;
            this.ajouter.Iconimage_Selected = null;
            this.ajouter.IconMarginLeft = 0;
            this.ajouter.IconMarginRight = 0;
            this.ajouter.IconRightVisible = true;
            this.ajouter.IconRightZoom = 0D;
            this.ajouter.IconVisible = true;
            this.ajouter.IconZoom = 74D;
            this.ajouter.IsTab = false;
            this.ajouter.Location = new System.Drawing.Point(107, 396);
            this.ajouter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ajouter.Name = "ajouter";
            this.ajouter.Normalcolor = System.Drawing.Color.Teal;
            this.ajouter.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.ajouter.OnHoverTextColor = System.Drawing.Color.White;
            this.ajouter.selected = false;
            this.ajouter.Size = new System.Drawing.Size(123, 31);
            this.ajouter.TabIndex = 124;
            this.ajouter.Text = "Ajouter";
            this.ajouter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ajouter.Textcolor = System.Drawing.Color.White;
            this.ajouter.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radioButton3);
            this.radGroupBox1.Controls.Add(this.radioButton2);
            this.radGroupBox1.Controls.Add(this.radioButton1);
            this.radGroupBox1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.HeaderText = "Etat de l\'année";
            this.radGroupBox1.Location = new System.Drawing.Point(94, 311);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(275, 77);
            this.radGroupBox1.TabIndex = 127;
            this.radGroupBox1.Text = "Etat de l\'année";
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).InvalidateMeasureInMainLayout = 2;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).BorderColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).BorderColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadGroupBoxElement)(this.radGroupBox1.GetChildAt(0))).BackColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radGroupBox1.GetChildAt(0).GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Standard;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).HighlightColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.GroupBoxHeader)(this.radGroupBox1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radGroupBox1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radGroupBox1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.ForeColor = System.Drawing.Color.Teal;
            this.radioButton2.Location = new System.Drawing.Point(109, 36);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(70, 19);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "ajourné";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Bold);
            this.radioButton1.ForeColor = System.Drawing.Color.Teal;
            this.radioButton1.Location = new System.Drawing.Point(19, 36);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(78, 19);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "en cours ";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.ForeColor = System.Drawing.Color.Teal;
            this.radioButton3.Location = new System.Drawing.Point(195, 36);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(60, 19);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "admit";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // Fcursus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(816, 440);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.supprimer);
            this.Controls.Add(this.modfier);
            this.Controls.Add(this.ajouter);
            this.Controls.Add(this.radDropDownList3);
            this.Controls.Add(this.radDropDownList2);
            this.Controls.Add(this.radDropDownList1);
            this.Controls.Add(this.lb4);
            this.Controls.Add(this.hide);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Controls.Add(this.gridTotal);
            this.Controls.Add(this.lb3);
            this.Controls.Add(this.lb2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Fcursus";
            this.Text = "fhistory";
            ((System.ComponentModel.ISupportInitialize)(this.gridTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Label lb3;
        private System.Windows.Forms.Label lb2;
        private Bunifu.Framework.UI.BunifuCustomDataGrid gridTotal;
        private System.Windows.Forms.BindingSource etudierBindingSource;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private System.Windows.Forms.Label hide;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.Label lb4;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList3;
        private Bunifu.Framework.UI.BunifuFlatButton supprimer;
        private Bunifu.Framework.UI.BunifuFlatButton modfier;
        private Bunifu.Framework.UI.BunifuFlatButton ajouter;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
    }
}