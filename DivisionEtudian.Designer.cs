﻿
using System.Windows.Forms;
namespace etablissmentt
{
    partial class DivisionEtudian
    {
        
            /// <summary> 
            /// Variable nécessaire au concepteur.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary> 
            /// Nettoyage des ressources utilisées.
            /// </summary>
            /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

           

            /// <summary> 
            /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
            /// le contenu de cette méthode avec l'éditeur de code.
            /// </summary>
            private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DivisionEtudian));
            this.gridetud = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.IdEtud = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.matriculeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDeNaissanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lieuDeNaissainceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageEtudiant = new System.Windows.Forms.DataGridViewImageColumn();
            this.etudiantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ecoledeformationDataSet = new etablissmentt.ecoledeformationDataSet();
            this.prenometud = new Telerik.WinControls.UI.RadTextBox();
            this.nometud = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateetud = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mat = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cls = new Telerik.WinControls.UI.RadDropDownList();
            this.classeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ecoledeformationDataSet1 = new etablissmentt.ecoledeformationDataSet();
            this.lieunaissetud = new Telerik.WinControls.UI.RadTextBox();
            this.emailetud = new Telerik.WinControls.UI.RadTextBox();
            this.tel = new Telerik.WinControls.UI.RadTextBox();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.impbtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.nv = new Telerik.WinControls.UI.RadDropDownList();
            this.nivscolBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.av = new Telerik.WinControls.UI.RadDropDownList();
            this.annscollBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.adressetud = new Telerik.WinControls.UI.RadTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.genre = new Telerik.Web.Design.RadioButtonGroup(this.components);
            this.lab = new System.Windows.Forms.Label();
            this.nivscolTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.nivscolTableAdapter();
            this.etudiantTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.etudiantTableAdapter();
            this.annscollTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.annscollTableAdapter();
            this.classeTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.classeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.gridetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prenometud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nometud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lieunaissetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nivscolBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.av)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.annscollBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adressetud)).BeginInit();
            this.genre.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridetud
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.gridetud.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridetud.AutoGenerateColumns = false;
            this.gridetud.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridetud.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridetud.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridetud.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSeaGreen;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridetud.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridetud.ColumnHeadersHeight = 42;
            this.gridetud.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdEtud,
            this.matriculeDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.prenomDataGridViewTextBoxColumn,
            this.dateDeNaissanceDataGridViewTextBoxColumn,
            this.lieuDeNaissainceDataGridViewTextBoxColumn,
            this.adresseDataGridViewTextBoxColumn,
            this.mailDataGridViewTextBoxColumn,
            this.genreDataGridViewTextBoxColumn,
            this.imageEtudiant});
            this.gridetud.DataSource = this.etudiantBindingSource;
            this.gridetud.DoubleBuffered = true;
            this.gridetud.EnableHeadersVisualStyles = false;
            this.gridetud.GridColor = System.Drawing.Color.Teal;
            this.gridetud.HeaderBgColor = System.Drawing.Color.Teal;
            this.gridetud.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridetud.Location = new System.Drawing.Point(479, 135);
            this.gridetud.Name = "gridetud";
            this.gridetud.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridetud.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridetud.RowHeadersWidth = 20;
            this.gridetud.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gridetud.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Teal;
            this.gridetud.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Teal;
            this.gridetud.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridetud.RowTemplate.Height = 42;
            this.gridetud.RowTemplate.ReadOnly = true;
            this.gridetud.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridetud.Size = new System.Drawing.Size(650, 377);
            this.gridetud.TabIndex = 0;
            this.gridetud.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.bunifuCustomDataGrid1_CellContentClick);
            this.gridetud.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.f);
            this.gridetud.CellStyleChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.t);
            this.gridetud.RowHeaderCellChanged += new System.Windows.Forms.DataGridViewRowEventHandler(this.selctiogrid);
            // 
            // IdEtud
            // 
            this.IdEtud.DataPropertyName = "IdEtud";
            this.IdEtud.HeaderText = "IdEtud";
            this.IdEtud.Name = "IdEtud";
            this.IdEtud.Visible = false;
            // 
            // matriculeDataGridViewTextBoxColumn
            // 
            this.matriculeDataGridViewTextBoxColumn.DataPropertyName = "matricule";
            this.matriculeDataGridViewTextBoxColumn.HeaderText = "matricule";
            this.matriculeDataGridViewTextBoxColumn.Name = "matriculeDataGridViewTextBoxColumn";
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "Nom";
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            // 
            // prenomDataGridViewTextBoxColumn
            // 
            this.prenomDataGridViewTextBoxColumn.DataPropertyName = "Prenom";
            this.prenomDataGridViewTextBoxColumn.HeaderText = "Prenom";
            this.prenomDataGridViewTextBoxColumn.Name = "prenomDataGridViewTextBoxColumn";
            // 
            // dateDeNaissanceDataGridViewTextBoxColumn
            // 
            this.dateDeNaissanceDataGridViewTextBoxColumn.DataPropertyName = "Date de naissance";
            this.dateDeNaissanceDataGridViewTextBoxColumn.HeaderText = "Date de naissance";
            this.dateDeNaissanceDataGridViewTextBoxColumn.Name = "dateDeNaissanceDataGridViewTextBoxColumn";
            // 
            // lieuDeNaissainceDataGridViewTextBoxColumn
            // 
            this.lieuDeNaissainceDataGridViewTextBoxColumn.DataPropertyName = "Lieu de naissaince";
            this.lieuDeNaissainceDataGridViewTextBoxColumn.HeaderText = "Lieu de naissaince";
            this.lieuDeNaissainceDataGridViewTextBoxColumn.Name = "lieuDeNaissainceDataGridViewTextBoxColumn";
            // 
            // adresseDataGridViewTextBoxColumn
            // 
            this.adresseDataGridViewTextBoxColumn.DataPropertyName = "adresse";
            this.adresseDataGridViewTextBoxColumn.HeaderText = "adresse";
            this.adresseDataGridViewTextBoxColumn.Name = "adresseDataGridViewTextBoxColumn";
            // 
            // mailDataGridViewTextBoxColumn
            // 
            this.mailDataGridViewTextBoxColumn.DataPropertyName = "mail";
            this.mailDataGridViewTextBoxColumn.HeaderText = "mail";
            this.mailDataGridViewTextBoxColumn.Name = "mailDataGridViewTextBoxColumn";
            // 
            // genreDataGridViewTextBoxColumn
            // 
            this.genreDataGridViewTextBoxColumn.DataPropertyName = "genre";
            this.genreDataGridViewTextBoxColumn.HeaderText = "genre";
            this.genreDataGridViewTextBoxColumn.Name = "genreDataGridViewTextBoxColumn";
            // 
            // imageEtudiant
            // 
            this.imageEtudiant.DataPropertyName = "imageEtudiant";
            this.imageEtudiant.HeaderText = "photo d\'edentité";
            this.imageEtudiant.Name = "imageEtudiant";
            // 
            // etudiantBindingSource
            // 
            this.etudiantBindingSource.DataMember = "etudiant";
            this.etudiantBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // ecoledeformationDataSet
            // 
            this.ecoledeformationDataSet.DataSetName = "ecoledeformationDataSet";
            this.ecoledeformationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // prenometud
            // 
            this.prenometud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.etudiantBindingSource, "Prenom", true));
            this.prenometud.Location = new System.Drawing.Point(280, 221);
            this.prenometud.Name = "prenometud";
            this.prenometud.Size = new System.Drawing.Size(180, 27);
            this.prenometud.TabIndex = 52;
            this.prenometud.TabStop = false;
            this.prenometud.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.h);
            this.prenometud.Leave += new System.EventHandler(this.rech);
            ((Telerik.WinControls.UI.RadTextBoxElement)(this.prenometud.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.prenometud.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.prenometud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // nometud
            // 
            this.nometud.BackColor = System.Drawing.Color.White;
            this.nometud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.etudiantBindingSource, "Nom", true));
            this.nometud.Location = new System.Drawing.Point(19, 221);
            this.nometud.Name = "nometud";
            this.nometud.Size = new System.Drawing.Size(180, 27);
            this.nometud.TabIndex = 51;
            this.nometud.TabStop = false;
            this.nometud.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.h);
            this.nometud.Leave += new System.EventHandler(this.rech);
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nometud.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nometud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nometud.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(275, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 25);
            this.label4.TabIndex = 50;
            this.label4.Text = "Prenom:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(14, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 30);
            this.label3.TabIndex = 49;
            this.label3.Text = "Nom:";
            // 
            // dateetud
            // 
            this.dateetud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.etudiantBindingSource, "Date de naissance", true));
            this.dateetud.ForeColor = System.Drawing.Color.Teal;
            this.dateetud.Location = new System.Drawing.Point(19, 303);
            this.dateetud.Name = "dateetud";
            // 
            // 
            // 
            this.dateetud.RootElement.ControlBounds = new System.Drawing.Rectangle(19, 303, 205, 25);
            this.dateetud.Size = new System.Drawing.Size(180, 24);
            this.dateetud.TabIndex = 80;
            this.dateetud.TabStop = false;
            this.dateetud.Text = "mercredi 27 mars 2019";
            this.dateetud.Value = new System.DateTime(2019, 3, 27, 0, 0, 0, 0);
            this.dateetud.Leave += new System.EventHandler(this.rech);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.dateetud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor3 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label7.ForeColor = System.Drawing.Color.Teal;
            this.label7.Location = new System.Drawing.Point(275, 263);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 25);
            this.label7.TabIndex = 77;
            this.label7.Text = "Lieu de naissnace:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(14, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 25);
            this.label5.TabIndex = 76;
            this.label5.Text = "Date de Naissace:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(14, 342);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 25);
            this.label1.TabIndex = 82;
            this.label1.Text = "E-mail:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(14, 424);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 25);
            this.label2.TabIndex = 84;
            this.label2.Text = "N° télephone:";
            // 
            // mat
            // 
            this.mat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mat.Location = new System.Drawing.Point(968, 58);
            this.mat.Name = "mat";
            this.mat.Size = new System.Drawing.Size(137, 27);
            this.mat.TabIndex = 96;
            this.mat.TabStop = false;
            this.mat.Click += new System.EventHandler(this.rehcerche_click);
            ((Telerik.WinControls.UI.RadTextBoxElement)(this.mat.GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.mat.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.mat.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.mat.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).LeftColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(964, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 25);
            this.label6.TabIndex = 95;
            this.label6.Text = "Maticule";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label8.ForeColor = System.Drawing.Color.Teal;
            this.label8.Location = new System.Drawing.Point(652, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 25);
            this.label8.TabIndex = 97;
            this.label8.Text = "Niveau scolaire";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Teal;
            this.label9.Location = new System.Drawing.Point(496, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(132, 25);
            this.label9.TabIndex = 98;
            this.label9.Text = "Année scolaire";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label10.ForeColor = System.Drawing.Color.Teal;
            this.label10.Location = new System.Drawing.Point(819, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 25);
            this.label10.TabIndex = 100;
            this.label10.Text = "Class";
            // 
            // cls
            // 
            this.cls.AutoScroll = true;
            this.cls.BackColor = System.Drawing.Color.Teal;
            this.cls.DataSource = this.classeBindingSource;
            this.cls.DisplayMember = "nomclass";
            this.cls.EnableKeyMap = true;
            this.cls.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold);
            this.cls.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cls.Location = new System.Drawing.Point(799, 61);
            this.cls.Name = "cls";
            this.cls.Size = new System.Drawing.Size(159, 28);
            this.cls.TabIndex = 99;
            this.cls.ValueMember = "idClass";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cls.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.cls.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.cls.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.cls.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.cls.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.cls.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // classeBindingSource
            // 
            this.classeBindingSource.DataMember = "classe";
            this.classeBindingSource.DataSource = this.ecoledeformationDataSet1;
            // 
            // ecoledeformationDataSet1
            // 
            this.ecoledeformationDataSet1.DataSetName = "ecoledeformationDataSet";
            this.ecoledeformationDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lieunaissetud
            // 
            this.lieunaissetud.BackColor = System.Drawing.Color.White;
            this.lieunaissetud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.etudiantBindingSource, "Lieu de naissaince", true));
            this.lieunaissetud.Location = new System.Drawing.Point(280, 300);
            this.lieunaissetud.Name = "lieunaissetud";
            this.lieunaissetud.Size = new System.Drawing.Size(180, 27);
            this.lieunaissetud.TabIndex = 103;
            this.lieunaissetud.TabStop = false;
            this.lieunaissetud.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.h);
            this.lieunaissetud.Leave += new System.EventHandler(this.rech);
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.lieunaissetud.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.lieunaissetud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lieunaissetud.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // emailetud
            // 
            this.emailetud.BackColor = System.Drawing.Color.White;
            this.emailetud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.etudiantBindingSource, "mail", true));
            this.emailetud.Location = new System.Drawing.Point(19, 384);
            this.emailetud.Name = "emailetud";
            this.emailetud.Size = new System.Drawing.Size(180, 27);
            this.emailetud.TabIndex = 52;
            this.emailetud.TabStop = false;
            this.emailetud.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.h);
            this.emailetud.Leave += new System.EventHandler(this.rech);
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.emailetud.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.emailetud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.emailetud.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // tel
            // 
            this.tel.BackColor = System.Drawing.Color.White;
            this.tel.Location = new System.Drawing.Point(20, 453);
            this.tel.Name = "tel";
            this.tel.Size = new System.Drawing.Size(180, 27);
            this.tel.TabIndex = 104;
            this.tel.TabStop = false;
            this.tel.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.h);
            this.tel.TextChanged += new System.EventHandler(this.radTextBox5_TextChanged);
            this.tel.Leave += new System.EventHandler(this.rech);
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.tel.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.tel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.tel.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tel.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 15;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(18, 536);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(123, 31);
            this.panel1.TabIndex = 108;
            this.panel1.Click += new System.EventHandler(this.ve);
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::etablissmentt.Properties.Resources.add11;
            this.pictureBox4.Location = new System.Drawing.Point(3, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(29, 24);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 109;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.ve);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label11.Location = new System.Drawing.Point(39, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ajouter";
            this.label11.Click += new System.EventHandler(this.ve);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Teal;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(153, 536);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(123, 31);
            this.panel2.TabIndex = 109;
            this.panel2.Click += new System.EventHandler(this.mod);
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::etablissmentt.Properties.Resources.white_pencil_icon_png_3_white_pencil_png_256_256;
            this.pictureBox2.Location = new System.Drawing.Point(3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(29, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 109;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.mod);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label12.Location = new System.Drawing.Point(36, 5);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "Modfier";
            this.label12.Click += new System.EventHandler(this.mod);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Teal;
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(287, 536);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(123, 31);
            this.panel3.TabIndex = 110;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::etablissmentt.Properties.Resources.waste_bin1;
            this.pictureBox5.Location = new System.Drawing.Point(3, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(29, 24);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 109;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(32, 4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Supprimer";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Teal;
            this.panel4.Controls.Add(this.pictureBox6);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel4.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(421, 536);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(131, 31);
            this.panel4.TabIndex = 111;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::etablissmentt.Properties.Resources.calendar_icon_white;
            this.pictureBox6.Location = new System.Drawing.Point(3, 4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(29, 24);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 109;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.absance);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label14.Location = new System.Drawing.Point(35, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "Abssnaces";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Teal;
            this.panel5.Controls.Add(this.pictureBox7);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel5.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.Location = new System.Drawing.Point(563, 536);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(123, 31);
            this.panel5.TabIndex = 112;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::etablissmentt.Properties.Resources.timeline_icon;
            this.pictureBox7.Location = new System.Drawing.Point(3, 4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(29, 24);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 109;
            this.pictureBox7.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label15.Location = new System.Drawing.Point(36, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "Cursus";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Teal;
            this.panel6.Controls.Add(this.pictureBox8);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel6.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel6.Location = new System.Drawing.Point(697, 536);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(123, 31);
            this.panel6.TabIndex = 113;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::etablissmentt.Properties.Resources.makefg;
            this.pictureBox8.Location = new System.Drawing.Point(3, 4);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(27, 24);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 109;
            this.pictureBox8.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label16.Location = new System.Drawing.Point(36, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "Notes";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Teal;
            this.panel7.Controls.Add(this.pictureBox9);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel7.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(831, 536);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(123, 31);
            this.panel7.TabIndex = 114;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::etablissmentt.Properties.Resources.Payment_01;
            this.pictureBox9.Location = new System.Drawing.Point(3, 4);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(29, 24);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 109;
            this.pictureBox9.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label17.Location = new System.Drawing.Point(26, 6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Veressment";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Teal;
            this.panel8.Controls.Add(this.pictureBox10);
            this.panel8.Controls.Add(this.label18);
            this.panel8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel8.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel8.Location = new System.Drawing.Point(965, 536);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(175, 31);
            this.panel8.TabIndex = 115;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::etablissmentt.Properties.Resources.mail2;
            this.pictureBox10.Location = new System.Drawing.Point(3, 4);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(29, 24);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 109;
            this.pictureBox10.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label18.Location = new System.Drawing.Point(36, 5);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(127, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Cantater Parent";
            // 
            // impbtn
            // 
            this.impbtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.impbtn.BackColor = System.Drawing.Color.Teal;
            this.impbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.impbtn.BorderRadius = 0;
            this.impbtn.ButtonText = "imprimer la sertificat de scolairité";
            this.impbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.impbtn.DisabledColor = System.Drawing.Color.Gray;
            this.impbtn.Iconcolor = System.Drawing.Color.Transparent;
            this.impbtn.Iconimage = ((System.Drawing.Image)(resources.GetObject("impbtn.Iconimage")));
            this.impbtn.Iconimage_right = null;
            this.impbtn.Iconimage_right_Selected = null;
            this.impbtn.Iconimage_Selected = null;
            this.impbtn.IconMarginLeft = 0;
            this.impbtn.IconMarginRight = 0;
            this.impbtn.IconRightVisible = true;
            this.impbtn.IconRightZoom = 0D;
            this.impbtn.IconVisible = true;
            this.impbtn.IconZoom = 55D;
            this.impbtn.IsTab = false;
            this.impbtn.Location = new System.Drawing.Point(0, 61);
            this.impbtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.impbtn.Name = "impbtn";
            this.impbtn.Normalcolor = System.Drawing.Color.Teal;
            this.impbtn.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.impbtn.OnHoverTextColor = System.Drawing.Color.White;
            this.impbtn.selected = false;
            this.impbtn.Size = new System.Drawing.Size(159, 52);
            this.impbtn.TabIndex = 106;
            this.impbtn.Text = "imprimer la sertificat de scolairité";
            this.impbtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.impbtn.Textcolor = System.Drawing.Color.White;
            this.impbtn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Teal;
            this.pictureBox3.ErrorImage = null;
            this.pictureBox3.Image = global::etablissmentt.Properties.Resources.cam;
            this.pictureBox3.Location = new System.Drawing.Point(158, 135);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(27, 22);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 105;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::etablissmentt.Properties.Resources._26_512;
            this.pictureBox1.Location = new System.Drawing.Point(158, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(130, 125);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.enter);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.sort);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Teal;
            this.panel9.Controls.Add(this.pictureBox11);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel9.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel9.Location = new System.Drawing.Point(672, 98);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(123, 31);
            this.panel9.TabIndex = 116;
            this.panel9.Click += new System.EventHandler(this.rehcerche_click);
            this.panel9.Paint += new System.Windows.Forms.PaintEventHandler(this.panel9_Paint);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Image = global::etablissmentt.Properties.Resources._66937556_magnifying_glass_lupe_icon_vector_illustration_graphic_design;
            this.pictureBox11.Location = new System.Drawing.Point(3, 4);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(29, 24);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 109;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Click += new System.EventHandler(this.rehcerche_click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label19.Location = new System.Drawing.Point(28, 5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(96, 20);
            this.label19.TabIndex = 0;
            this.label19.Text = "Rechercher";
            this.label19.Click += new System.EventHandler(this.rehcerche_click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Teal;
            this.panel10.Controls.Add(this.pictureBox12);
            this.panel10.Controls.Add(this.label20);
            this.panel10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel10.Font = new System.Drawing.Font("Century Schoolbook", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel10.Location = new System.Drawing.Point(815, 98);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(123, 31);
            this.panel10.TabIndex = 117;
            this.panel10.Click += new System.EventHandler(this.actualisation);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::etablissmentt.Properties.Resources.united_states_win_the_white_house_hotel_business_company_refresh_icon_thumb1;
            this.pictureBox12.Location = new System.Drawing.Point(3, 4);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(29, 24);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 109;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Click += new System.EventHandler(this.actualisation);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label20.Location = new System.Drawing.Point(37, 5);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 20);
            this.label20.TabIndex = 0;
            this.label20.Text = "Actualiser";
            this.label20.Click += new System.EventHandler(this.actualisation);
            // 
            // nv
            // 
            this.nv.BackColor = System.Drawing.Color.Teal;
            this.nv.DataSource = this.nivscolBindingSource;
            this.nv.DisplayMember = "nomNS";
            this.nv.EnableKeyMap = true;
            this.nv.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold);
            this.nv.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.nv.Location = new System.Drawing.Point(651, 61);
            this.nv.Name = "nv";
            this.nv.Size = new System.Drawing.Size(138, 28);
            this.nv.TabIndex = 118;
            this.nv.ValueMember = "idNiveauScolaire";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.nv.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.nv.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nv.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.nv.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.nv.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nv.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // nivscolBindingSource
            // 
            this.nivscolBindingSource.DataMember = "nivscol";
            this.nivscolBindingSource.DataSource = this.ecoledeformationDataSet1;
            // 
            // av
            // 
            this.av.BackColor = System.Drawing.Color.Teal;
            this.av.DataSource = this.annscollBindingSource;
            this.av.DisplayMember = "codeann";
            this.av.EnableKeyMap = true;
            this.av.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.av.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.av.Location = new System.Drawing.Point(503, 61);
            this.av.Name = "av";
            // 
            // 
            // 
            this.av.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(53)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.av.Size = new System.Drawing.Size(138, 28);
            this.av.TabIndex = 119;
            this.av.ValueMember = "idas";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.av.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.av.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.av.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.av.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.av.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.av.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.LightVisualButtonElement)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(3).GetChildAt(0))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.LightVisualButtonElement)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(3).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.av.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // annscollBindingSource
            // 
            this.annscollBindingSource.DataMember = "annscoll";
            this.annscollBindingSource.DataSource = this.ecoledeformationDataSet1;
            // 
            // adressetud
            // 
            this.adressetud.BackColor = System.Drawing.Color.White;
            this.adressetud.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.etudiantBindingSource, "adresse", true));
            this.adressetud.Location = new System.Drawing.Point(280, 384);
            this.adressetud.Name = "adressetud";
            this.adressetud.Size = new System.Drawing.Size(180, 27);
            this.adressetud.TabIndex = 121;
            this.adressetud.TabStop = false;
            this.adressetud.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.h);
            this.adressetud.TextChanged += new System.EventHandler(this.adressetud_TextChanged);
            this.adressetud.Leave += new System.EventHandler(this.rech);
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.adressetud.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.adressetud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.adressetud.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label21.ForeColor = System.Drawing.Color.Teal;
            this.label21.Location = new System.Drawing.Point(275, 347);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(150, 25);
            this.label21.TabIndex = 120;
            this.label21.Text = "Adresse Actuale:";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(18, 41);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(91, 24);
            this.radioButton1.TabIndex = 122;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Garçon";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(123, 41);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(66, 24);
            this.radioButton2.TabIndex = 123;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Fille";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // genre
            // 
            this.genre.AutoSize = true;
            this.genre.Controls.Add(this.radioButton1);
            this.genre.Controls.Add(this.radioButton2);
            this.genre.DataSource = null;
            this.genre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.genre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genre.ForeColor = System.Drawing.Color.Teal;
            this.genre.Location = new System.Drawing.Point(269, 424);
            this.genre.Name = "genre";
            this.genre.Size = new System.Drawing.Size(195, 91);
            this.genre.TabIndex = 82;
            this.genre.TabStop = false;
            this.genre.Text = "Genre:";
            // 
            // lab
            // 
            this.lab.AutoSize = true;
            this.lab.BackColor = System.Drawing.Color.Teal;
            this.lab.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lab.Location = new System.Drawing.Point(181, 136);
            this.lab.Name = "lab";
            this.lab.Size = new System.Drawing.Size(65, 20);
            this.lab.TabIndex = 122;
            this.lab.Text = "modfier";
            this.lab.Visible = false;
            this.lab.Click += new System.EventHandler(this.lab_Click);
            // 
            // nivscolTableAdapter
            // 
            this.nivscolTableAdapter.ClearBeforeFill = true;
            // 
            // etudiantTableAdapter
            // 
            this.etudiantTableAdapter.ClearBeforeFill = true;
            // 
            // annscollTableAdapter
            // 
            this.annscollTableAdapter.ClearBeforeFill = true;
            // 
            // classeTableAdapter
            // 
            this.classeTableAdapter.ClearBeforeFill = true;
            // 
            // DivisionEtudian
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lab);
            this.Controls.Add(this.genre);
            this.Controls.Add(this.adressetud);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.av);
            this.Controls.Add(this.nv);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.impbtn);
            this.Controls.Add(this.tel);
            this.Controls.Add(this.emailetud);
            this.Controls.Add(this.lieunaissetud);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cls);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.mat);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateetud);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.prenometud);
            this.Controls.Add(this.nometud);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.gridetud);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "DivisionEtudian";
            this.Size = new System.Drawing.Size(1172, 587);
            this.Load += new System.EventHandler(this.DivisionEtudian_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prenometud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nometud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lieunaissetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nivscolBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.av)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.annscollBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adressetud)).EndInit();
            this.genre.ResumeLayout(false);
            this.genre.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

            }

   

            private Bunifu.Framework.UI.BunifuCustomDataGrid gridetud;
            private System.Windows.Forms.PictureBox pictureBox1;
            private Telerik.WinControls.UI.RadTextBox prenometud;
            private Telerik.WinControls.UI.RadTextBox nometud;
            private System.Windows.Forms.Label label4;
            private System.Windows.Forms.Label label3;
            private Telerik.WinControls.UI.RadDateTimePicker dateetud;
            private System.Windows.Forms.Label label7;
            private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadTextBox mat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadDropDownList cls;
        private Telerik.WinControls.UI.RadTextBox lieunaissetud;
        private Telerik.WinControls.UI.RadTextBox emailetud;
        private Telerik.WinControls.UI.RadTextBox tel;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Bunifu.Framework.UI.BunifuFlatButton impbtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.BindingSource etudiantBindingSource;
        private ecoledeformationDataSet ecoledeformationDataSet;
        private ecoledeformationDataSetTableAdapters.etudiantTableAdapter etudiantTableAdapter;
        private System.Windows.Forms.DataGridViewImageColumn imageEtudiantDataGridViewImageColumn;
        private Telerik.WinControls.UI.RadDropDownList av;
        private Telerik.WinControls.UI.RadDropDownList nv;
        private Telerik.WinControls.UI.RadTextBox adressetud;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdEtud;
        private System.Windows.Forms.DataGridViewTextBoxColumn matriculeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDeNaissanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lieuDeNaissainceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adresseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn genreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn imageEtudiant;
        private RadioButton radioButton1;
        private RadioButton radioButton2;
        private Telerik.Web.Design.RadioButtonGroup genre;
        private Label lab;
        private BindingSource annscollBindingSource;
        private ecoledeformationDataSet ecoledeformationDataSet1;
        private ecoledeformationDataSetTableAdapters.annscollTableAdapter annscollTableAdapter;
        private BindingSource classeBindingSource;
        private BindingSource nivscolBindingSource;
        private ecoledeformationDataSetTableAdapters.nivscolTableAdapter nivscolTableAdapter;
        private ecoledeformationDataSetTableAdapters.classeTableAdapter classeTableAdapter;
    }
    }
