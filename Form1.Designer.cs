﻿namespace etablissmentt
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

      

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.idEtudDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDeNaissanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lieuDeNaissainceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doubleBitmapControl1 = new BunifuAnimatorNS.DoubleBitmapControl();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.Inter = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.X = new System.Windows.Forms.Label();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.p5 = new System.Windows.Forms.Panel();
            this.p4 = new System.Windows.Forms.Panel();
            this.p3 = new System.Windows.Forms.Panel();
            this.p2 = new System.Windows.Forms.Panel();
            this.p1 = new System.Windows.Forms.Panel();
            this.tre = new Telerik.WinControls.UI.RadButton();
            this.stat = new Telerik.WinControls.UI.RadButton();
            this.mat = new Telerik.WinControls.UI.RadButton();
            this.Profisseur = new Telerik.WinControls.UI.RadButton();
            this.Etud = new Telerik.WinControls.UI.RadButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bunifuTransition1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.matiers1 = new etablissmentt.matiers();
            this.divisionProf1 = new etablissmentt.divisionProf();
            this.divisionEtudian = new etablissmentt.DivisionEtudian();
            this.etudiantTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.etudiantTableAdapter();
            this.etudiantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ecoledeformationDataSet = new etablissmentt.ecoledeformationDataSet();
            this.ecoledeformationDataSet1 = new etablissmentt.ecoledeformationDataSet();
            this.professeurBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.professeurTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.professeurTableAdapter();
            this.annscollTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.annscollTableAdapter();
            this.nivscolBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nivscolTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.nivscolTableAdapter();
            this.classeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.classeTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.classeTableAdapter();
            this.annscollBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.matiereBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.matiereTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.matiereTableAdapter();
            this.examBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.examTableAdapter = new etablissmentt.ecoledeformationDataSetTableAdapters.examTableAdapter();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.nivscolTableAdapter1 = new etablissmentt.ecoledeformationDataSetTableAdapters.nivscolTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.bunifuGradientPanel2.SuspendLayout();
            this.bunifuGradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Profisseur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Etud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.professeurBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nivscolBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.annscollBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matiereBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // idEtudDataGridViewTextBoxColumn
            // 
            this.idEtudDataGridViewTextBoxColumn.DataPropertyName = "IdEtud";
            this.idEtudDataGridViewTextBoxColumn.HeaderText = "IdEtud";
            this.idEtudDataGridViewTextBoxColumn.Name = "idEtudDataGridViewTextBoxColumn";
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "Nom";
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            // 
            // prenomDataGridViewTextBoxColumn
            // 
            this.prenomDataGridViewTextBoxColumn.DataPropertyName = "Prenom";
            this.prenomDataGridViewTextBoxColumn.HeaderText = "Prenom";
            this.prenomDataGridViewTextBoxColumn.Name = "prenomDataGridViewTextBoxColumn";
            // 
            // dateDeNaissanceDataGridViewTextBoxColumn
            // 
            this.dateDeNaissanceDataGridViewTextBoxColumn.DataPropertyName = "Date de naissance";
            this.dateDeNaissanceDataGridViewTextBoxColumn.HeaderText = "Date de naissance";
            this.dateDeNaissanceDataGridViewTextBoxColumn.Name = "dateDeNaissanceDataGridViewTextBoxColumn";
            // 
            // lieuDeNaissainceDataGridViewTextBoxColumn
            // 
            this.lieuDeNaissainceDataGridViewTextBoxColumn.DataPropertyName = "Lieu de naissaince";
            this.lieuDeNaissainceDataGridViewTextBoxColumn.HeaderText = "Lieu de naissaince";
            this.lieuDeNaissainceDataGridViewTextBoxColumn.Name = "lieuDeNaissainceDataGridViewTextBoxColumn";
            // 
            // adresseDataGridViewTextBoxColumn
            // 
            this.adresseDataGridViewTextBoxColumn.DataPropertyName = "adresse";
            this.adresseDataGridViewTextBoxColumn.HeaderText = "adresse";
            this.adresseDataGridViewTextBoxColumn.Name = "adresseDataGridViewTextBoxColumn";
            // 
            // mailDataGridViewTextBoxColumn
            // 
            this.mailDataGridViewTextBoxColumn.DataPropertyName = "mail";
            this.mailDataGridViewTextBoxColumn.HeaderText = "mail";
            this.mailDataGridViewTextBoxColumn.Name = "mailDataGridViewTextBoxColumn";
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "IdEtud";
            this.dataGridViewTextBoxColumn1.HeaderText = "IdEtud";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nom";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nom";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Prenom";
            this.dataGridViewTextBoxColumn3.HeaderText = "Prenom";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Date de naissance";
            this.dataGridViewTextBoxColumn4.HeaderText = "Date de naissance";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Lieu de naissaince";
            this.dataGridViewTextBoxColumn5.HeaderText = "Lieu de naissaince";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "adresse";
            this.dataGridViewTextBoxColumn6.HeaderText = "adresse";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "mail";
            this.dataGridViewTextBoxColumn7.HeaderText = "mail";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // doubleBitmapControl1
            // 
            this.bunifuTransition1.SetDecoration(this.doubleBitmapControl1, BunifuAnimatorNS.DecorationType.None);
            this.doubleBitmapControl1.Location = new System.Drawing.Point(0, 0);
            this.doubleBitmapControl1.Name = "doubleBitmapControl1";
            this.doubleBitmapControl1.Size = new System.Drawing.Size(0, 0);
            this.doubleBitmapControl1.TabIndex = 0;
            this.doubleBitmapControl1.Text = "doubleBitmapControl1";
            this.doubleBitmapControl1.Visible = false;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 8;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.Inter);
            this.bunifuGradientPanel2.Controls.Add(this.label1);
            this.bunifuGradientPanel2.Controls.Add(this.X);
            this.bunifuTransition1.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.Teal;
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.Teal;
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.Teal;
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(151, 0);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(1180, 46);
            this.bunifuGradientPanel2.TabIndex = 2;
            // 
            // Inter
            // 
            this.Inter.AutoSize = true;
            this.Inter.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.Inter, BunifuAnimatorNS.DecorationType.None);
            this.Inter.Font = new System.Drawing.Font("Maiandra GD", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Inter.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Inter.Location = new System.Drawing.Point(6, 5);
            this.Inter.Name = "Inter";
            this.Inter.Size = new System.Drawing.Size(264, 36);
            this.Inter.TabIndex = 5;
            this.Inter.Text = "Inetrface Etudiant";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(1091, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 42);
            this.label1.TabIndex = 1;
            this.label1.Text = "-";
            // 
            // X
            // 
            this.X.AutoSize = true;
            this.X.BackColor = System.Drawing.Color.Transparent;
            this.X.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.X, BunifuAnimatorNS.DecorationType.None);
            this.X.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.X.Location = new System.Drawing.Point(1123, 9);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(35, 32);
            this.X.TabIndex = 0;
            this.X.Text = "X";
            this.X.Click += new System.EventHandler(this.X_Click);
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.Controls.Add(this.p5);
            this.bunifuGradientPanel1.Controls.Add(this.p4);
            this.bunifuGradientPanel1.Controls.Add(this.p3);
            this.bunifuGradientPanel1.Controls.Add(this.p2);
            this.bunifuGradientPanel1.Controls.Add(this.p1);
            this.bunifuGradientPanel1.Controls.Add(this.tre);
            this.bunifuGradientPanel1.Controls.Add(this.stat);
            this.bunifuGradientPanel1.Controls.Add(this.mat);
            this.bunifuGradientPanel1.Controls.Add(this.Profisseur);
            this.bunifuGradientPanel1.Controls.Add(this.Etud);
            this.bunifuGradientPanel1.Controls.Add(this.pictureBox1);
            this.bunifuTransition1.SetDecoration(this.bunifuGradientPanel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.Teal;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Teal;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(151, 637);
            this.bunifuGradientPanel1.TabIndex = 1;
            this.bunifuGradientPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.bunifuGradientPanel1_Paint);
            // 
            // p5
            // 
            this.p5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuTransition1.SetDecoration(this.p5, BunifuAnimatorNS.DecorationType.None);
            this.p5.Location = new System.Drawing.Point(0, 416);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(15, 30);
            this.p5.TabIndex = 14;
            this.p5.Visible = false;
            // 
            // p4
            // 
            this.p4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuTransition1.SetDecoration(this.p4, BunifuAnimatorNS.DecorationType.None);
            this.p4.Location = new System.Drawing.Point(0, 371);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(15, 30);
            this.p4.TabIndex = 13;
            this.p4.Visible = false;
            // 
            // p3
            // 
            this.p3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuTransition1.SetDecoration(this.p3, BunifuAnimatorNS.DecorationType.None);
            this.p3.Location = new System.Drawing.Point(0, 326);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(15, 30);
            this.p3.TabIndex = 12;
            this.p3.Visible = false;
            // 
            // p2
            // 
            this.p2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuTransition1.SetDecoration(this.p2, BunifuAnimatorNS.DecorationType.None);
            this.p2.Location = new System.Drawing.Point(0, 281);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(15, 30);
            this.p2.TabIndex = 11;
            this.p2.Visible = false;
            // 
            // p1
            // 
            this.p1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuTransition1.SetDecoration(this.p1, BunifuAnimatorNS.DecorationType.None);
            this.p1.Location = new System.Drawing.Point(0, 236);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(15, 30);
            this.p1.TabIndex = 0;
            // 
            // tre
            // 
            this.tre.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.tre, BunifuAnimatorNS.DecorationType.None);
            this.tre.Location = new System.Drawing.Point(12, 416);
            this.tre.Name = "tre";
            this.tre.Size = new System.Drawing.Size(136, 30);
            this.tre.TabIndex = 10;
            this.tre.Text = "Trésorie";
            this.tre.Click += new System.EventHandler(this.tre_Click);
            this.tre.MouseLeave += new System.EventHandler(this.leave5);
            this.tre.MouseHover += new System.EventHandler(this.hove5);
            ((Telerik.WinControls.UI.RadButtonElement)(this.tre.GetChildAt(0))).Text = "Trésorie";
            ((Telerik.WinControls.UI.RadButtonElement)(this.tre.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.tre.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.tre.GetChildAt(0))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadButtonElement)(this.tre.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 10F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.tre.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.tre.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.tre.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.tre.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.tre.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.tre.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.tre.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tre.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tre.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tre.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tre.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.tre.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // stat
            // 
            this.stat.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.stat, BunifuAnimatorNS.DecorationType.None);
            this.stat.Location = new System.Drawing.Point(12, 371);
            this.stat.Name = "stat";
            this.stat.Size = new System.Drawing.Size(136, 30);
            this.stat.TabIndex = 9;
            this.stat.Text = "Statistique";
            this.stat.Click += new System.EventHandler(this.stat_Click);
            this.stat.MouseLeave += new System.EventHandler(this.leave4);
            this.stat.MouseHover += new System.EventHandler(this.hove4);
            ((Telerik.WinControls.UI.RadButtonElement)(this.stat.GetChildAt(0))).Text = "Statistique";
            ((Telerik.WinControls.UI.RadButtonElement)(this.stat.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.stat.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.stat.GetChildAt(0))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadButtonElement)(this.stat.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 10F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.stat.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.stat.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.stat.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.stat.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.stat.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.stat.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.stat.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.stat.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.stat.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.stat.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.stat.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.stat.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // mat
            // 
            this.mat.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.mat, BunifuAnimatorNS.DecorationType.None);
            this.mat.Location = new System.Drawing.Point(12, 326);
            this.mat.Name = "mat";
            this.mat.Size = new System.Drawing.Size(136, 30);
            this.mat.TabIndex = 8;
            this.mat.Text = "Matiers";
            this.mat.Click += new System.EventHandler(this.mat_Click);
            this.mat.MouseLeave += new System.EventHandler(this.leave3);
            this.mat.MouseHover += new System.EventHandler(this.hove3);
            ((Telerik.WinControls.UI.RadButtonElement)(this.mat.GetChildAt(0))).Text = "Matiers";
            ((Telerik.WinControls.UI.RadButtonElement)(this.mat.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.mat.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.mat.GetChildAt(0))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadButtonElement)(this.mat.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 10F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.mat.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.mat.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.mat.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.mat.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.mat.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.mat.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.mat.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.mat.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // Profisseur
            // 
            this.Profisseur.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.Profisseur, BunifuAnimatorNS.DecorationType.None);
            this.Profisseur.Location = new System.Drawing.Point(12, 281);
            this.Profisseur.Name = "Profisseur";
            this.Profisseur.Size = new System.Drawing.Size(136, 30);
            this.Profisseur.TabIndex = 7;
            this.Profisseur.Text = "Profisseur";
            this.Profisseur.Click += new System.EventHandler(this.Profisseur_Click);
            this.Profisseur.MouseLeave += new System.EventHandler(this.leave2);
            this.Profisseur.MouseHover += new System.EventHandler(this.hove2);
            ((Telerik.WinControls.UI.RadButtonElement)(this.Profisseur.GetChildAt(0))).Text = "Profisseur";
            ((Telerik.WinControls.UI.RadButtonElement)(this.Profisseur.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.Profisseur.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.Profisseur.GetChildAt(0))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadButtonElement)(this.Profisseur.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 10F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Profisseur.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // Etud
            // 
            this.Etud.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.Etud, BunifuAnimatorNS.DecorationType.None);
            this.Etud.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Etud.Location = new System.Drawing.Point(12, 236);
            this.Etud.Name = "Etud";
            this.Etud.Size = new System.Drawing.Size(136, 30);
            this.Etud.TabIndex = 6;
            this.Etud.Text = "Etudiant";
            this.Etud.Click += new System.EventHandler(this.Etud_Click);
            this.Etud.MouseEnter += new System.EventHandler(this.djd);
            this.Etud.MouseLeave += new System.EventHandler(this.leave);
            this.Etud.MouseHover += new System.EventHandler(this.hove);
            ((Telerik.WinControls.UI.RadButtonElement)(this.Etud.GetChildAt(0))).Text = "Etudiant";
            ((Telerik.WinControls.UI.RadButtonElement)(this.Etud.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.Etud.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadButtonElement)(this.Etud.GetChildAt(0))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.RadButtonElement)(this.Etud.GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 10F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Etud.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Etud.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Etud.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Etud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Etud.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 20F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.Etud.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.Etud.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.Etud.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Etud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Etud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Etud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Etud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.Etud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Transparent;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox1.Image = global::etablissmentt.Properties.Resources.k;
            this.pictureBox1.Location = new System.Drawing.Point(1, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(147, 143);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // bunifuTransition1
            // 
            this.bunifuTransition1.AnimationType = BunifuAnimatorNS.AnimationType.VertSlide;
            this.bunifuTransition1.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.bunifuTransition1.DefaultAnimation = animation1;
            this.bunifuTransition1.MaxAnimationTime = 2000;
            // 
            // matiers1
            // 
            this.matiers1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuTransition1.SetDecoration(this.matiers1, BunifuAnimatorNS.DecorationType.None);
            this.matiers1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.matiers1.Location = new System.Drawing.Point(151, 46);
            this.matiers1.Name = "matiers1";
            this.matiers1.Size = new System.Drawing.Size(1180, 591);
            this.matiers1.TabIndex = 5;
            this.matiers1.Visible = false;
            this.matiers1.Load += new System.EventHandler(this.matiers1_Load);
            // 
            // divisionProf1
            // 
            this.divisionProf1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuTransition1.SetDecoration(this.divisionProf1, BunifuAnimatorNS.DecorationType.None);
            this.divisionProf1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divisionProf1.Location = new System.Drawing.Point(151, 46);
            this.divisionProf1.Name = "divisionProf1";
            this.divisionProf1.Size = new System.Drawing.Size(1180, 591);
            this.divisionProf1.TabIndex = 4;
            this.divisionProf1.Visible = false;
            this.divisionProf1.Load += new System.EventHandler(this.divisionProf1_Load);
            // 
            // divisionEtudian
            // 
            this.divisionEtudian.BackColor = System.Drawing.Color.WhiteSmoke;
            this.divisionEtudian.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuTransition1.SetDecoration(this.divisionEtudian, BunifuAnimatorNS.DecorationType.None);
            this.divisionEtudian.Dock = System.Windows.Forms.DockStyle.Fill;
            this.divisionEtudian.Location = new System.Drawing.Point(151, 46);
            this.divisionEtudian.Name = "divisionEtudian";
            this.divisionEtudian.Size = new System.Drawing.Size(1180, 591);
            this.divisionEtudian.TabIndex = 3;
            // 
            // etudiantTableAdapter
            // 
            this.etudiantTableAdapter.ClearBeforeFill = true;
            // 
            // etudiantBindingSource
            // 
            this.etudiantBindingSource.DataMember = "etudiant";
            this.etudiantBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // ecoledeformationDataSet
            // 
            this.ecoledeformationDataSet.DataSetName = "ecoledeformationDataSet";
            this.ecoledeformationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ecoledeformationDataSet1
            // 
            this.ecoledeformationDataSet1.DataSetName = "ecoledeformationDataSet";
            this.ecoledeformationDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // professeurBindingSource
            // 
            this.professeurBindingSource.DataMember = "professeur";
            this.professeurBindingSource.DataSource = this.ecoledeformationDataSet1;
            // 
            // professeurTableAdapter
            // 
            this.professeurTableAdapter.ClearBeforeFill = true;
            // 
            // annscollTableAdapter
            // 
            this.annscollTableAdapter.ClearBeforeFill = true;
            // 
            // nivscolBindingSource
            // 
            this.nivscolBindingSource.DataMember = "nivscol";
            this.nivscolBindingSource.DataSource = this.ecoledeformationDataSet1;
            // 
            // nivscolTableAdapter
            // 
            this.nivscolTableAdapter.ClearBeforeFill = true;
            // 
            // classeBindingSource
            // 
            this.classeBindingSource.DataMember = "classe";
            this.classeBindingSource.DataSource = this.ecoledeformationDataSet1;
            // 
            // classeTableAdapter
            // 
            this.classeTableAdapter.ClearBeforeFill = true;
            // 
            // annscollBindingSource
            // 
            this.annscollBindingSource.DataMember = "annscoll";
            this.annscollBindingSource.DataSource = this.ecoledeformationDataSet1;
            // 
            // matiereBindingSource
            // 
            this.matiereBindingSource.DataMember = "matiere";
            this.matiereBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // matiereTableAdapter
            // 
            this.matiereTableAdapter.ClearBeforeFill = true;
            // 
            // examBindingSource
            // 
            this.examBindingSource.DataMember = "exam";
            this.examBindingSource.DataSource = this.ecoledeformationDataSet;
            // 
            // examTableAdapter
            // 
            this.examTableAdapter.ClearBeforeFill = true;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "nivscol";
            this.bindingSource1.DataSource = this.ecoledeformationDataSet;
            // 
            // nivscolTableAdapter1
            // 
            this.nivscolTableAdapter1.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1331, 637);
            this.Controls.Add(this.matiers1);
            this.Controls.Add(this.divisionProf1);
            this.Controls.Add(this.divisionEtudian);
            this.Controls.Add(this.bunifuGradientPanel2);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.bunifuTransition1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            this.bunifuGradientPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Profisseur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Etud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ecoledeformationDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.professeurBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nivscolBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.annscollBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matiereBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

       
        private System.Windows.Forms.DataGridViewTextBoxColumn idEtudDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDeNaissanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lieuDeNaissainceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adresseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mailDataGridViewTextBoxColumn;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private BunifuAnimatorNS.DoubleBitmapControl doubleBitmapControl1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label X;
        private Telerik.WinControls.UI.RadButton tre;
        private Telerik.WinControls.UI.RadButton stat;
        private Telerik.WinControls.UI.RadButton mat;
        private Telerik.WinControls.UI.RadButton Profisseur;
        private Telerik.WinControls.UI.RadButton Etud;
        private System.Windows.Forms.Panel p5;
        private System.Windows.Forms.Panel p4;
        private System.Windows.Forms.Panel p3;
        private System.Windows.Forms.Panel p2;
        private System.Windows.Forms.Panel p1;
        private divisionProf divisionProf1;
        private DivisionEtudian divisionEtudian;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition1;
        private System.Windows.Forms.Label Inter;
        private ecoledeformationDataSetTableAdapters.etudiantTableAdapter etudiantTableAdapter;
        private System.Windows.Forms.BindingSource etudiantBindingSource;
        private ecoledeformationDataSet ecoledeformationDataSet;
        private ecoledeformationDataSet ecoledeformationDataSet1;
        private System.Windows.Forms.BindingSource professeurBindingSource;
        private ecoledeformationDataSetTableAdapters.professeurTableAdapter professeurTableAdapter;
        private ecoledeformationDataSetTableAdapters.annscollTableAdapter annscollTableAdapter;
        private System.Windows.Forms.BindingSource nivscolBindingSource;
        private ecoledeformationDataSetTableAdapters.nivscolTableAdapter nivscolTableAdapter;
        private System.Windows.Forms.BindingSource classeBindingSource;
        private ecoledeformationDataSetTableAdapters.classeTableAdapter classeTableAdapter;
        private System.Windows.Forms.BindingSource annscollBindingSource;
        private matiers matiers1;
        private System.Windows.Forms.BindingSource matiereBindingSource;
        private ecoledeformationDataSetTableAdapters.matiereTableAdapter matiereTableAdapter;
        private System.Windows.Forms.BindingSource examBindingSource;
        private ecoledeformationDataSetTableAdapters.examTableAdapter examTableAdapter;
        private System.Windows.Forms.BindingSource bindingSource1;
        private ecoledeformationDataSetTableAdapters.nivscolTableAdapter nivscolTableAdapter1;
    }
}

