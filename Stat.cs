﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace etablissmentt
{
    public partial class Stat : UserControl
    {
        public Stat()
        {
            InitializeComponent();
        }

        private void Stat_Load(object sender, EventArgs e)
        {
            etudiantTableAdapter.Fill(ecoledeformationDataSet.etudiant, "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%", "%%",  DateTime.Parse("01/01/1000"), DateTime.Parse("01/01/3000"), "%%");
            abssanceTableAdapter.Fill(ecoledeformationDataSet.abssance, "%%", "%%");
            examTableAdapter.Fill(ecoledeformationDataSet.exam);
            matiereTableAdapter.Fill(ecoledeformationDataSet.matiere,"%%");
            cmb_etudiant.Text = "";
            dtp_debut.Value = DateTime.Now;
            dtp_fin.Value = DateTime.Now;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmb_etudiant_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_etudiant.Text != "") 
            {
                if (cmb_etudiant.SelectedValue != null) 
                {
                    abssanceTableAdapter.Fill(ecoledeformationDataSet.abssance,"%%",cmb_etudiant.SelectedValue.ToString());
                }
            }
        }

        private void cmb_nv_scolaire_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_nv_scolaire.Text != "") 
            {
                if (cmb_nv_scolaire.SelectedValue != null) 
                {
                    chart_nb_et_cl.DataSource = etudierTableAdapter.GetDataBy("2019",cmb_nv_scolaire.SelectedValue.ToString());
                    chart_nb_et_cl.Series["Series"].XValueMember = "Classe";
                    chart_nb_et_cl.Series["Series"].YValueMembers = "Expr1";

                }
            }
        }

        private void bsearch_Click(object sender, EventArgs e)
        {
            string etudiant = "%%"; DateTime date_debut = DateTime.Parse("01/01/1000"), date_fin = DateTime.Parse("01/01/3000");

            if (cmb_etudiant.Text != "") 
            {
                if (cmb_etudiant.SelectedValue != null) etudiant = cmb_etudiant.SelectedValue.ToString();
            }

            abssanceTableAdapter.Fill(ecoledeformationDataSet.abssance,"%%",etudiant);
        }
    }
}
