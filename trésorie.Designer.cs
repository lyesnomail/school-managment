﻿namespace etablissmentt
{
    partial class trésorie

    { 



            /// <summary> 
            /// Variable nécessaire au concepteur.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary> 
            /// Nettoyage des ressources utilisées.
            /// </summary>
            /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }



            /// <summary> 
            /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
            /// le contenu de cette méthode avec l'éditeur de code.
            /// </summary>
            private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bunifuCustomDataGrid1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.prenometud = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateetud = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox5 = new Telerik.WinControls.UI.RadTextBox();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.nometud = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuCustomDataGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prenometud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nometud)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuCustomDataGrid1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuCustomDataGrid1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.bunifuCustomDataGrid1.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.bunifuCustomDataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bunifuCustomDataGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bunifuCustomDataGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.bunifuCustomDataGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bunifuCustomDataGrid1.DoubleBuffered = true;
            this.bunifuCustomDataGrid1.EnableHeadersVisualStyles = false;
            this.bunifuCustomDataGrid1.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.bunifuCustomDataGrid1.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.bunifuCustomDataGrid1.Location = new System.Drawing.Point(666, 89);
            this.bunifuCustomDataGrid1.Name = "bunifuCustomDataGrid1";
            this.bunifuCustomDataGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.bunifuCustomDataGrid1.RowTemplate.Height = 24;
            this.bunifuCustomDataGrid1.Size = new System.Drawing.Size(489, 423);
            this.bunifuCustomDataGrid1.TabIndex = 0;
            // 
            // prenometud
            // 
            this.prenometud.Location = new System.Drawing.Point(295, 221);
            this.prenometud.Name = "prenometud";
            this.prenometud.Size = new System.Drawing.Size(180, 27);
            this.prenometud.TabIndex = 52;
            this.prenometud.TabStop = false;
            ((Telerik.WinControls.UI.RadTextBoxElement)(this.prenometud.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.prenometud.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.prenometud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.prenometud.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label4.ForeColor = System.Drawing.Color.Teal;
            this.label4.Location = new System.Drawing.Point(435, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 25);
            this.label4.TabIndex = 50;
            this.label4.Text = "Prenom:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(20, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 30);
            this.label3.TabIndex = 49;
            this.label3.Text = "Nom:";
            // 
            // dateetud
            // 
            this.dateetud.ForeColor = System.Drawing.Color.Teal;
            this.dateetud.Location = new System.Drawing.Point(19, 115);
            this.dateetud.Name = "dateetud";
            // 
            // 
            // 
            this.dateetud.RootElement.ControlBounds = new System.Drawing.Rectangle(19, 115, 205, 25);
            this.dateetud.Size = new System.Drawing.Size(180, 24);
            this.dateetud.TabIndex = 80;
            this.dateetud.TabStop = false;
            this.dateetud.Text = "mercredi 27 mars 2019";
            this.dateetud.Value = new System.DateTime(2019, 3, 27, 0, 0, 0, 0);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.dateetud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor3 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label7.ForeColor = System.Drawing.Color.Teal;
            this.label7.Location = new System.Drawing.Point(290, 263);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 25);
            this.label7.TabIndex = 77;
            this.label7.Text = "Lieu de naissnace:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(14, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 25);
            this.label5.TabIndex = 76;
            this.label5.Text = "Date de Naissace:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(14, 342);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 25);
            this.label1.TabIndex = 82;
            this.label1.Text = "E-mail:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(14, 424);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 25);
            this.label2.TabIndex = 84;
            this.label2.Text = "grade:";
            // 
            // radTextBox4
            // 
            this.radTextBox4.BackColor = System.Drawing.Color.White;
            this.radTextBox4.Location = new System.Drawing.Point(295, 300);
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.Size = new System.Drawing.Size(180, 27);
            this.radTextBox4.TabIndex = 103;
            this.radTextBox4.TabStop = false;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radTextBox4.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radTextBox4.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox4.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // radTextBox1
            // 
            this.radTextBox1.BackColor = System.Drawing.Color.White;
            this.radTextBox1.Location = new System.Drawing.Point(19, 384);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(180, 27);
            this.radTextBox1.TabIndex = 52;
            this.radTextBox1.TabStop = false;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radTextBox1.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radTextBox1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // radTextBox5
            // 
            this.radTextBox5.BackColor = System.Drawing.Color.White;
            this.radTextBox5.Location = new System.Drawing.Point(18, 452);
            this.radTextBox5.Name = "radTextBox5";
            this.radTextBox5.Size = new System.Drawing.Size(180, 27);
            this.radTextBox5.TabIndex = 104;
            this.radTextBox5.TabStop = false;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radTextBox5.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radTextBox5.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox5.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 15;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.Teal;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Actualiser";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = global::etablissmentt.Properties.Resources.refresh_white_192x192;
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 80D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(865, 40);
            this.bunifuFlatButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.Teal;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(121, 31);
            this.bunifuFlatButton1.TabIndex = 102;
            this.bunifuFlatButton1.Text = "Actualiser";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.Teal;
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = "Rechercher";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = global::etablissmentt.Properties.Resources._66937556_magnifying_glass_lupe_icon_vector_illustration_graphic_design;
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 60D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(715, 40);
            this.bunifuFlatButton2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.Teal;
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(131, 31);
            this.bunifuFlatButton2.TabIndex = 101;
            this.bunifuFlatButton2.Text = "Rechercher";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // nometud
            // 
            this.nometud.BackColor = System.Drawing.Color.White;
            this.nometud.Location = new System.Drawing.Point(25, 196);
            this.nometud.Name = "nometud";
            this.nometud.Size = new System.Drawing.Size(180, 27);
            this.nometud.TabIndex = 51;
            this.nometud.TabStop = false;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nometud.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nometud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nometud.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // trésorie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.radTextBox5);
            this.Controls.Add(this.radTextBox1);
            this.Controls.Add(this.radTextBox4);
            this.Controls.Add(this.bunifuFlatButton1);
            this.Controls.Add(this.bunifuFlatButton2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateetud);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.prenometud);
            this.Controls.Add(this.nometud);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bunifuCustomDataGrid1);
            this.Name = "trésorie";
            this.Size = new System.Drawing.Size(1172, 587);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuCustomDataGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prenometud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nometud)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

            }



            private Bunifu.Framework.UI.BunifuCustomDataGrid bunifuCustomDataGrid1;
            private Telerik.WinControls.UI.RadTextBox prenometud;
            private System.Windows.Forms.Label label4;
            private System.Windows.Forms.Label label3;
            private Telerik.WinControls.UI.RadDateTimePicker dateetud;
            private System.Windows.Forms.Label label7;
            private System.Windows.Forms.Label label5;
            private System.Windows.Forms.Label label1;
            private System.Windows.Forms.Label label2;
            private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
            private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
            private Telerik.WinControls.UI.RadTextBox radTextBox4;
            private Telerik.WinControls.UI.RadTextBox radTextBox1;
            private Telerik.WinControls.UI.RadTextBox radTextBox5;
            private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Telerik.WinControls.UI.RadTextBox nometud;
    }
    }


