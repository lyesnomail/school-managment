﻿using System.Windows.Forms;

namespace etablissmentt
{
    partial class Fpayment
    {
        
        

            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }



            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fpayment));
            this.lb2 = new System.Windows.Forms.Label();
            this.gridTotal = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.hide = new System.Windows.Forms.Label();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.modfier = new Bunifu.Framework.UI.BunifuFlatButton();
            this.ajouter = new Bunifu.Framework.UI.BunifuFlatButton();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.lb5 = new System.Windows.Forms.Label();
            this.lb3 = new System.Windows.Forms.Label();
            this.lb6 = new System.Windows.Forms.Label();
            this.radDropDownList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.nometud = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.dateetud = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nometud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).BeginInit();
            this.SuspendLayout();
            // 
            // lb2
            // 
            this.lb2.AutoSize = true;
            this.lb2.BackColor = System.Drawing.Color.Transparent;
            this.lb2.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb2.ForeColor = System.Drawing.Color.Teal;
            this.lb2.Location = new System.Drawing.Point(97, 18);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(209, 38);
            this.lb2.TabIndex = 70;
            this.lb2.Text = "Année scollaire:";
            // 
            // gridTotal
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridTotal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridTotal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridTotal.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridTotal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridTotal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTotal.DoubleBuffered = true;
            this.gridTotal.EnableHeadersVisualStyles = false;
            this.gridTotal.HeaderBgColor = System.Drawing.Color.Teal;
            this.gridTotal.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
            this.gridTotal.Location = new System.Drawing.Point(375, 74);
            this.gridTotal.Name = "gridTotal";
            this.gridTotal.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.gridTotal.RowTemplate.Height = 24;
            this.gridTotal.Size = new System.Drawing.Size(429, 341);
            this.gridTotal.TabIndex = 76;
            // 
            // hide
            // 
            this.hide.AutoSize = true;
            this.hide.BackColor = System.Drawing.Color.Transparent;
            this.hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hide.ForeColor = System.Drawing.Color.Teal;
            this.hide.Location = new System.Drawing.Point(725, 10);
            this.hide.Name = "hide";
            this.hide.Size = new System.Drawing.Size(34, 44);
            this.hide.TabIndex = 117;
            this.hide.Text = "-";
            // 
            // radLabel1
            // 
            this.radLabel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radLabel1.Font = new System.Drawing.Font("Gill Sans MT", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.Teal;
            this.radLabel1.Location = new System.Drawing.Point(766, 13);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(31, 33);
            this.radLabel1.TabIndex = 116;
            this.radLabel1.Text = "X";
            // 
            // modfier
            // 
            this.modfier.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.modfier.BackColor = System.Drawing.Color.Teal;
            this.modfier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.modfier.BorderRadius = 0;
            this.modfier.ButtonText = "Modfier";
            this.modfier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modfier.DisabledColor = System.Drawing.Color.Gray;
            this.modfier.Iconcolor = System.Drawing.Color.Transparent;
            this.modfier.Iconimage = null;
            this.modfier.Iconimage_right = null;
            this.modfier.Iconimage_right_Selected = null;
            this.modfier.Iconimage_Selected = null;
            this.modfier.IconMarginLeft = 0;
            this.modfier.IconMarginRight = 0;
            this.modfier.IconRightVisible = true;
            this.modfier.IconRightZoom = 0D;
            this.modfier.IconVisible = true;
            this.modfier.IconZoom = 62D;
            this.modfier.IsTab = false;
            this.modfier.Location = new System.Drawing.Point(242, 396);
            this.modfier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.modfier.Name = "modfier";
            this.modfier.Normalcolor = System.Drawing.Color.Teal;
            this.modfier.OnHovercolor = System.Drawing.Color.Gold;
            this.modfier.OnHoverTextColor = System.Drawing.Color.White;
            this.modfier.selected = false;
            this.modfier.Size = new System.Drawing.Size(123, 31);
            this.modfier.TabIndex = 125;
            this.modfier.Text = "Modfier";
            this.modfier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.modfier.Textcolor = System.Drawing.Color.White;
            this.modfier.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // ajouter
            // 
            this.ajouter.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.ajouter.BackColor = System.Drawing.Color.Teal;
            this.ajouter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ajouter.BorderRadius = 0;
            this.ajouter.ButtonText = "Ajouter";
            this.ajouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ajouter.DisabledColor = System.Drawing.Color.Gray;
            this.ajouter.Iconcolor = System.Drawing.Color.Transparent;
            this.ajouter.Iconimage = null;
            this.ajouter.Iconimage_right = null;
            this.ajouter.Iconimage_right_Selected = null;
            this.ajouter.Iconimage_Selected = null;
            this.ajouter.IconMarginLeft = 0;
            this.ajouter.IconMarginRight = 0;
            this.ajouter.IconRightVisible = true;
            this.ajouter.IconRightZoom = 0D;
            this.ajouter.IconVisible = true;
            this.ajouter.IconZoom = 74D;
            this.ajouter.IsTab = false;
            this.ajouter.Location = new System.Drawing.Point(107, 396);
            this.ajouter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ajouter.Name = "ajouter";
            this.ajouter.Normalcolor = System.Drawing.Color.Teal;
            this.ajouter.OnHovercolor = System.Drawing.Color.LightSeaGreen;
            this.ajouter.OnHoverTextColor = System.Drawing.Color.White;
            this.ajouter.selected = false;
            this.ajouter.Size = new System.Drawing.Size(123, 31);
            this.ajouter.TabIndex = 124;
            this.ajouter.Text = "Ajouter";
            this.ajouter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ajouter.Textcolor = System.Drawing.Color.White;
            this.ajouter.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.BackColor = System.Drawing.Color.Teal;
            this.radDropDownList1.EnableKeyMap = true;
            radListDataItem1.Text = "controle";
            radListDataItem2.Text = "exmain finale";
            this.radDropDownList1.Items.Add(radListDataItem1);
            this.radDropDownList1.Items.Add(radListDataItem2);
            this.radDropDownList1.Location = new System.Drawing.Point(104, 60);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(251, 24);
            this.radDropDownList1.TabIndex = 121;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList1.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList1.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList1.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList1.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            // 
            // lb5
            // 
            this.lb5.AutoSize = true;
            this.lb5.BackColor = System.Drawing.Color.Transparent;
            this.lb5.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb5.ForeColor = System.Drawing.Color.Teal;
            this.lb5.Location = new System.Drawing.Point(97, 161);
            this.lb5.Name = "lb5";
            this.lb5.Size = new System.Drawing.Size(226, 38);
            this.lb5.TabIndex = 128;
            this.lb5.Text = "Date de payment:";
            // 
            // lb3
            // 
            this.lb3.AutoSize = true;
            this.lb3.BackColor = System.Drawing.Color.Transparent;
            this.lb3.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb3.ForeColor = System.Drawing.Color.Teal;
            this.lb3.Location = new System.Drawing.Point(97, 88);
            this.lb3.Name = "lb3";
            this.lb3.Size = new System.Drawing.Size(165, 38);
            this.lb3.TabIndex = 127;
            this.lb3.Text = "Information:";
            // 
            // lb6
            // 
            this.lb6.AutoSize = true;
            this.lb6.BackColor = System.Drawing.Color.Transparent;
            this.lb6.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb6.ForeColor = System.Drawing.Color.Teal;
            this.lb6.Location = new System.Drawing.Point(97, 231);
            this.lb6.Name = "lb6";
            this.lb6.Size = new System.Drawing.Size(129, 38);
            this.lb6.TabIndex = 129;
            this.lb6.Text = "Methode:";
            // 
            // radDropDownList2
            // 
            this.radDropDownList2.BackColor = System.Drawing.Color.Teal;
            this.radDropDownList2.EnableKeyMap = true;
            radListDataItem3.Text = "controle";
            radListDataItem4.Text = "exmain finale";
            this.radDropDownList2.Items.Add(radListDataItem3);
            this.radDropDownList2.Items.Add(radListDataItem4);
            this.radDropDownList2.Location = new System.Drawing.Point(104, 273);
            this.radDropDownList2.Name = "radDropDownList2";
            this.radDropDownList2.Size = new System.Drawing.Size(251, 24);
            this.radDropDownList2.TabIndex = 130;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).RightToLeft = false;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList2.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList2.GetChildAt(0).GetChildAt(2))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(2))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor3 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).InnerColor4 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.WhiteSmoke;
            // 
            // nometud
            // 
            this.nometud.BackColor = System.Drawing.Color.White;
            this.nometud.Location = new System.Drawing.Point(104, 130);
            this.nometud.Name = "nometud";
            this.nometud.Size = new System.Drawing.Size(251, 27);
            this.nometud.TabIndex = 131;
            this.nometud.TabStop = false;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nometud.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.nometud.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.nometud.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.nometud.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // radTextBox1
            // 
            this.radTextBox1.BackColor = System.Drawing.Color.White;
            this.radTextBox1.Location = new System.Drawing.Point(104, 343);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(251, 27);
            this.radTextBox1.TabIndex = 132;
            this.radTextBox1.TabStop = false;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radTextBox1.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radTextBox1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BorderDrawMode = Telerik.WinControls.BorderDrawModes.LeftOverTop;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).Width = 1F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BottomWidth = 5F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BottomColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).InnerColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radTextBox1.GetChildAt(0).GetChildAt(2))).Enabled = false;
            // 
            // dateetud
            // 
            this.dateetud.ForeColor = System.Drawing.Color.Teal;
            this.dateetud.Location = new System.Drawing.Point(104, 203);
            this.dateetud.Name = "dateetud";
            // 
            // 
            // 
            this.dateetud.RootElement.ControlBounds = new System.Drawing.Rectangle(104, 203, 251, 24);
            this.dateetud.Size = new System.Drawing.Size(251, 24);
            this.dateetud.TabIndex = 133;
            this.dateetud.TabStop = false;
            this.dateetud.Text = "mercredi 27 mars 2019";
            this.dateetud.Value = new System.DateTime(2019, 3, 27, 0, 0, 0, 0);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.dateetud.GetChildAt(0).GetChildAt(2))).ForeColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor2 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor3 = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(0))).BackColor = System.Drawing.Color.Teal;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dateetud.GetChildAt(0).GetChildAt(2).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.Teal;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Teal;
            this.label1.Location = new System.Drawing.Point(97, 301);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 38);
            this.label1.TabIndex = 134;
            this.label1.Text = "Montant";
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.Teal;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Teal;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.LightSeaGreen;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(88, 447);
            this.bunifuGradientPanel1.TabIndex = 77;
            // 
            // Fpayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(816, 440);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateetud);
            this.Controls.Add(this.radTextBox1);
            this.Controls.Add(this.nometud);
            this.Controls.Add(this.radDropDownList2);
            this.Controls.Add(this.lb6);
            this.Controls.Add(this.lb3);
            this.Controls.Add(this.lb5);
            this.Controls.Add(this.modfier);
            this.Controls.Add(this.ajouter);
            this.Controls.Add(this.radDropDownList1);
            this.Controls.Add(this.hide);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Controls.Add(this.gridTotal);
            this.Controls.Add(this.lb2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Fpayment";
            ((System.ComponentModel.ISupportInitialize)(this.gridTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nometud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateetud)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

            }
            private System.Windows.Forms.Label lb2;
            private Bunifu.Framework.UI.BunifuCustomDataGrid gridTotal;
            private System.Windows.Forms.BindingSource etudierBindingSource;
            private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
            private System.Windows.Forms.Label hide;
            private Telerik.WinControls.UI.RadLabel radLabel1;
            private Bunifu.Framework.UI.BunifuFlatButton modfier;
            private Bunifu.Framework.UI.BunifuFlatButton ajouter;
            private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
            private Label lb5;
            private Label lb3;
            private Label lb6;
            private Telerik.WinControls.UI.RadDropDownList radDropDownList2;
            private Telerik.WinControls.UI.RadTextBox nometud;
            private Telerik.WinControls.UI.RadTextBox radTextBox1;
            private Telerik.WinControls.UI.RadDateTimePicker dateetud;
            private Label label1;

            
        }
    }